Software:
https://docs.conda.io/projects/miniconda/en/latest/
https://brew.sh
https://supercollider.github.io/downloads.html#mac

1. Clone git repo:
https://gitlab.ub.uni-bielefeld.de/marco.adamczyk/pychaoson.git

2. Go Inside Repo
cd pychaoson

2. Create conda env
conda env create -f environment.yml

3. Activate env
conda activate audioenv

4. Setup development
python setup.py develop

5. Open Jupyter lab
Jupyter lab

