import pytest 
import pychaoson.sample_edit as sample_edit
import os
import pychaoson.config as config
import shutil

test_dir = os.path.dirname(os.path.abspath(__file__))
# Define the path to the test data directory relative to the script
test_data_dir = os.path.join(test_dir, 'test_data')
test_samples_dir = os.path.join(test_dir, 'test_samples')


def test_edit_sample():
    # setup
    sample_files = [os.path.join(test_samples_dir, file) for file in os.listdir(test_samples_dir)]
    sample_file_names = ['LinearSin', 'PnoHarpStrumMono']
    if not os.path.exists(test_data_dir):
        os.mkdir(test_data_dir)
    

    # execute
    samples_dict = sample_edit.get_samples_dict(sample_files , 48000, reload=True, get_files=False)

    # verify
    all_notes = config.get_midi_note_list()
    for sample_file_name in sample_file_names:
        # test for every sample that the sample is actually in the result dict
        assert sample_file_name in samples_dict.keys() 
        sample_dict =  samples_dict[sample_file_name]
        # assert that all notes are coverd
        assert list(sample_dict.keys()) == all_notes

    # teardown
    shutil.rmtree(test_data_dir)