import pytest
import pychaoson.pychaoson as pychaoson
import pychaoson.config as config
from itertools import product
import sc3nb
import shutil
import os

test_dir = os.path.dirname(os.path.abspath(__file__))

# Define the path to the test data directory relative to the script
test_data_dir = os.path.join(test_dir, 'test_data')
test_samples_dir = os.path.join(test_dir, 'test_samples')

@pytest.fixture(scope="function")
def pychaoson_instance():
    # Setup
    if not os.path.exists(test_data_dir):
        os.mkdir(test_data_dir)
    pychaos = pychaoson.startup(test_data_dir, test_samples_dir)
    yield pychaos  # Provide the Calculator instance to the test
    # Teardown
    pychaos.stop()
    shutil.rmtree(test_data_dir)

#with pytest.raises(ValueError):
#    divide(5, 0)
expected_chaotic_systems = ['Hennon Map', 'Logistic Map', 'Standard Map']
expected_playtypes = ['Granular', 'Chaotic']
expected_samples = ['LinearSin', 'PnoHarpStrumMono']
init_test_cases = list(product(expected_chaotic_systems, expected_playtypes, expected_samples))

@pytest.mark.parametrize('chaotic_system_name, playtype, sample_name', init_test_cases)
def test_correct_synth_init(pychaoson_instance, chaotic_system_name, playtype, sample_name):
    # run
    synth = pychaoson_instance.start_synth(chaotic_system_name, playtype, sample_name)
    # validate
    # assert that all variables are set correctly
    assert synth is not None
    assert synth.current_sample_name == sample_name
    assert synth.chaotic_system.get_name() == chaotic_system_name
    assert synth.playtype == playtype
    
    for midi_note in config.get_midi_note_list():
        # if granular assert that for every note a buffer was allocaed
        if playtype == 'Granular':
            buf = synth.buffer_dict[midi_note]
            assert type(buf) == sc3nb.Buffer
            assert buf._allocated == True
            assert synth.bufnum_dict[midi_note] == buf._bufnum 
        # check for every note that a synthdef was allocated
        assert type(synth.synth_def_dict[midi_note]) == sc3nb.SynthDef
        assert type(synth.synth_name_dict[midi_note]) == str

    # assert that the chaotic sliders were initialized correctly
    assert len(synth.chaotic_sliders_dict.keys()) == len(pychaoson_instance.chaotic_systems_dict[chaotic_system_name].get_param_space())
    for param_dim in synth.chaotic_system.get_param_space():
        assert param_dim[3] in synth.chaotic_sliders_dict.keys()
        chaotic_slider = synth.chaotic_sliders_dict[param_dim[3]]
        assert chaotic_slider.get_start_value() == param_dim[0]
        assert chaotic_slider.get_min_value() == param_dim[1]
        assert chaotic_slider.get_max_value() == param_dim[2]
    # assert that the chaotic sliders were initialized correctly
    assert 'Amp' in list(synth.custom_sliders_dict.keys())
    assert 'Reverb Mix' in list(synth.custom_sliders_dict.keys())
    if playtype == 'Granular':
        assert 'Grain Length' in list(synth.custom_sliders_dict.keys())
        assert 'Grain Overlap' in list(synth.custom_sliders_dict.keys())


wrong_init_test_cases = [('Hennon Map', 'Granular', None),
                         ('Hennon Mpa', 'Granular', 'LinearSin'),
                         ('Hennon Map', 'Granulr', 'LinearSin'),
                         ('Hennon Map', 'Granular', 'LinearSine')]

@pytest.mark.parametrize("chaotic_system_name, playtype, sample_name", wrong_init_test_cases)
def test_false_synth_init(pychaoson_instance, chaotic_system_name, playtype, sample_name):
    # assert that the exeception is thrown as exepted
    with pytest.raises(Exception):
        pychaoson_instance.start_synth(chaotic_system_name, playtype, sample_name)

def test_synth_playing(pychaoson_instance ):
    # setup
    synth = pychaoson_instance.start_synth('Hennon Map', 'Chaotic', 'LinearSin')
    midi_note_1 = 40
    new_value = 1.5
    # execute
    synth.play_note(midi_note_1)
    # validate
    # assert synth was created
    assert midi_note_1 in synth.synth_dict.keys()
    assert len(synth.synth_dict.keys()) == 1
    sc_synth = synth.synth_dict[midi_note_1]
    assert type(sc_synth) == sc3nb.Synth

    # execute
    synth.set_current_param('Dim0', 1.5, midi_note_1)

    # validate
    # assert synth still there
    assert midi_note_1 in synth.synth_dict.keys()
    assert len(synth.synth_dict.keys()) == 1
    sc_synth = synth.synth_dict[midi_note_1]
    assert type(sc_synth) == sc3nb.Synth
    # assert current points changed for the note in the sliders
    current_points = synth.get_chaos_current_param([0])
    assert len(current_points) == 1
    assert new_value == current_points[0][0]

    # execute
    synth.release_note(midi_note_1)

    # validate
    # check synth is no longer available
    assert midi_note_1 not in synth.synth_dict.keys()
    assert len(synth.synth_dict.keys()) == 0
    current_points = synth.get_chaos_current_param([0])
    assert len(current_points) == 0