# PyChaoSon
## Table of Contents

1. [About](##about)
2. [Getting Started](#getting-started)
3. [Usage](#usage)
4. [License](#license)

## About
PyChaoSon is a python package that offers a sythesizer that explores the parameter spaces of chaotic systems.
##### Features:
* Available Synthesis methods
    * Using the caotic systems to select the grains of a custom sample
    * Direct synthesis of the chaotic system in audio rate
* Sensitivity calibrated parameter spaces
* Graphical User Interface wrapping the api control
* Dimension calculation and visualization of chaotic system parameter spaces
* Controlable by a [roli seaboard](https://roli.com/products/blocks/seaboard-block-studio-edition)
* Saving patches

##### This project is part of the bachelor thesis "Development of sensitivity-calibrated multi-parameter Control of Chaos-based Synthesizers for Musical Expression"
##### Thesis Description:
In the bachelor thesis, selected chaotic systems are to be analyzed with respect to their sonic properties when used as synthesizers (sonification). Thereby especially the benefit in the application in musical instruments should be worked out. By analysis and visualization it should be possible to select interesting areas of the parameter space and to control them via sensitivity calibration with adjustable map magnification by the playing parameters of a multidimensional MIDI keyboard (ROLI Blocks). 
The development should be done in Python and applications should be realized as interactive Jupyter notebooks, making user interfaces available via ipywidgets and displaying relevant states visually as well.


## Getting Started
* A SuperCollider installation on your system in required.ownload.
* Create a python3 environment. Possible Installations:
    * [miniconda](https://docs.conda.io/projects/miniconda/en/latest/) (recommended)
    * [venv](https://docs.python.org/3/library/venv.html)
    * [python standalone](https://www.python.org/downloads/)

### Use as package
TODO

### Develop
[Git](https://git-scm.com) required

1. Clone git repo to the desired place
2. Navigate to repo 
```bash
cd pychaoson
```
3. Install dependencies:
     - conda 
     ```bash
     conda env create -f environment.yml` 
     conda activate audioenv
    ```
    - pip
    ```bash
    pip install -r requirements.txt
    ```
4. Call the setup.py method
```bash
python setup.py develop
```
5. Happy coding :)

## Usage
For practical usages and experimenting it is recommended to use the implemented gui with ipywidgets and matplotlib. \
The api can be used to embed the project into a new project or to create a new ui.



### Control with API

### Control with GUI in Jupyter Notebooks


## License
This project is licensed under the Mit License - see the [LICENSE.md](LICENSE.txt) file for details.
