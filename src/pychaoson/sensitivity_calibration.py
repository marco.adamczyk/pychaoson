import numpy as np


def __normalize__(value, min_value, max_value):
    return 0 + ((1 - 0) / (max_value - min_value)) * (value - min_value)


def __denormalize__(value, min_value, max_value):
    return min_value + ((max_value - min_value) / (1 - 0)) * (value - 0)


class PointOfInterest:
    """
    This class stores the points in the parameter space that neighbourhoodd should be scale so that space around that 
    point becomes more dense.
    """
    def __init__(self, poi, radius, exp=2, repelling=False):
        """
        Initialzes the class by setting the parameter of that point its radius and the exp to which the space should get distorted.  

        Parameters
        ----------
        poi : [float]
             The interesting point. list length should suit the parameter space dimension
        radius : float
            The radius to wich extend points should be affected. 1 is the full parameter space extend. 0 is nothing.
        exp: int
            Define which exp function should be use for the transformation only exp % 2 = 0 values allowed.
        """
        self.poi = poi
        self.radius = radius
        self.exp = exp
        self.repelling = repelling
        self.vector_length_func = self.__polynomial__

    def get_vector_new(self, point):
        """
        Computes the shift that a point in the parameter space has to make.

        Parameters
        ----------
        point : [float]
             The interesting point. list length should suit the parameter space dimension

        Returns
        -------
        vector: [float]
            A vector that represents the point shift
        """
        # get the vector between those points
        vector_to_poi = np.subtract(point, self.poi)
        # magnitude/length of vector
        distance_to_poi = np.linalg.norm(vector_to_poi)
        # only add a non-zero vector if the point is under the sphere of influence
        if distance_to_poi == 0:
            return np.zeros(len(point)).tolist()
        if distance_to_poi < self.radius:
            vector_poi_radius = np.multiply(self.radius/distance_to_poi, vector_to_poi)
            x = np.multiply(vector_poi_radius, self.vector_length_func(distance_to_poi, 0, self.radius, self.exp, self.repelling)/self.radius)  
            return np.subtract(np.add(x, self.poi), point)
            #x_1_ = self.radius - distance_to_poi
            #x_1 = x_1_ / (2 * self.radius)
            ##x_2 = self.vector_length_func(x_1, 0.5, 0.5, self.exp, self.repelling)
            #x_r_ = np.subtract(point, self.poi)
            #x_r = np.add(np.multiply(self.radius/np.linalg.norm(x_r_) ,x_r_), self.poi)
            #x_3 = np.add(x_r, np.multiply(2*x_2, np.subtract(self.poi, x_r)))
            #return np.subtract(x_3, point)
        else:
            return np.zeros(len(point)).tolist()
        

    def __polynomial__(self, x, pos, radius, exp, repelling):
        a = (radius)/(radius ** exp)
        if not repelling:
            result = a * (x - pos) ** exp + pos
        else:
            #computing inverse
            if x <= pos:
                x_val = (x - pos) / a
                result = ((-(-x_val) ** (1/exp))*1j).imag + pos
            else:
                result = ((x - pos)/a) ** (1 / exp) + pos

        if exp % 2 == 0 and x < pos and not repelling:
            result = (-(result - pos)) + pos
        return result



def transform_start_values(chaos_sliders, poi_list):
    """
    Transform the start values of the sliders that represent the parameter space of the chaotic system.

    Parameters
    ----------
    chaos_sliders : [Sliders]
        Sliders in a list that represent the parameter space.
    poi_list : [PointOfInterest]
        A list of Point Of interest object that are influencing the transform result

    Returns
    -------
    vector: [float]
        A new shifted point
    """
    normalized_point = normalize_start_point(chaos_sliders)
    selected_vector = np.zeros((len(normalized_point)))
    for poi in poi_list:
        selected_vector = np.add(selected_vector, poi.get_vector_new(normalized_point))
    # shift the parameters
    normalized_transformed_values = np.add(normalized_point, selected_vector)
    return denormalize_point(chaos_sliders, normalized_transformed_values)

def transform_custom_values(values, chaos_sliders, poi_list):
    """
    Transform custom values that lie in the parameter space of the chaotic system.

    Parameters
    ----------
    values: [float]
        Represents the point in parameter space.
    chaos_sliders: [Sliders]
        Sliders in a list that represent the parameter space.
    poi_list: [PointOfInterest]
        A list of Point Of interest object that are influencing the transform result

    Returns
    -------
    vector: [float]
        A new shifted point
    """
    normalized_point = normalize_custom_point(values, chaos_sliders)
    selected_vector = np.zeros((len(normalized_point)))
    for poi in poi_list:
        selected_vector = np.add(selected_vector, poi.get_vector_new(normalized_point))
    # shift the parameters
    normalized_transformed_values = np.add(normalized_point, selected_vector)
    return denormalize_point(chaos_sliders, normalized_transformed_values)


def transform_current_values(chaos_sliders, poi_list, key):
    """
    Transform the current values of the sliders that represent the parameter space of the chaotic system.

    Parameters
    ----------
    chaos_sliders : [Sliders]
        Sliders in a list that represent the parameter space.
    poi_list : [PointOfInterest]
        A list of Point Of interest object that are influencing the transform result

    Returns
    -------
    vector: [float]
        A new shifted point
    """
    normalized_values = normalize_current_point(chaos_sliders, key)
    vector_shift = np.zeros((len(normalized_values)))
    for poi in poi_list:
        vector_shift = np.add(vector_shift, poi.get_vector_new(normalized_values))
    # shift the parameters
    normalized_transformed_values = np.add(normalized_values, vector_shift)
    return denormalize_point(chaos_sliders, normalized_transformed_values)

def normalize_current_point(chaos_sliders, key):
    """
    Normalizes the current point of the sliders at a given key. 

    Parameters
    ----------
    chaos_sliders : [Sliders]
        Sliders in a list that represent the parameter space.
    key : int
        The specific key

    Returns
    -------
    vector: [float]
        A list that represents the normalized version of the point
    """
    return [__normalize__(chaos_sliders[i].get_current_value(key), chaos_sliders[i].get_min_value(), chaos_sliders[i].get_max_value())
            for i in range(len(chaos_sliders))]

def normalize_start_point(chaos_sliders):
    """
    Normalizes the start point of the sliders.

    Parameters
    ----------
    chaos_sliders : [Sliders]
        Sliders in a list that represent the parameter space.

    Returns
    -------
    vector: [float]
        A list that represents the normalized version of the point
    """
    return [__normalize__(chaos_sliders[i].get_start_value(), chaos_sliders[i].get_min_value(), chaos_sliders[i].get_max_value())
            for i in range(len(chaos_sliders))]

def normalize_custom_point(values, chaos_sliders):
    """
    Normalizes a point that lies in the parameters space of the sliders.

    Parameters
    ----------
    values : [float]
        A list of float that represent the a point in parameter space
    chaos_sliders : [Sliders]
        Sliders in a list that represent the parameter space.

    Returns
    -------
    vector: [float]
        A vector that represents the point shift
    """
    return [__normalize__(values[i], chaos_sliders[i].get_min_value(), chaos_sliders[i].get_max_value())
            for i in range(len(chaos_sliders))]

def denormalize_point(chaos_sliders, point):
    """
    Reverts the normalization of a point in normalized parameter space

    Parameters
    ----------
    point : [float]
         A list of float that represents the a point in normalized parameter space

    Returns
    -------
    vector: [float]
        A list of float that represents the a point in the original parameter space
    """
    return [__denormalize__(point[i], chaos_sliders[i].get_min_value(), chaos_sliders[i].get_max_value())
            for i in range(len(point))]

def get_points_of_interest_dict(poi_list):
    """
    A dict of the interesting points
    Is used for storing the current state

    Parameters
    ----------
    poi_list : [PointOfInterest]
         A list of point of interest objects.

    Return
    ----------
    pois_dict_list : list(dict)
        A list of dictionaries that store the information of all the point of interesting objects
    """
    pois_dict_list = []
    for poi in poi_list:
        poi_dict = dict()
        poi_dict['poi'] = poi.poi
        poi_dict['radius'] = poi.radius
        poi_dict['exp'] = poi.exp
        pois_dict_list.append(poi_dict)
    return pois_dict_list