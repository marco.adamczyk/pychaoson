import os
import sys
from contextlib import contextmanager
from os import listdir
from os.path import isfile, join, splitext, basename


def load_files_from_directory(directory):
    """
    Extracts every file from a directory with its full path

    Parameters
    ----------
    directory : str
         The path to the directory

    Return
    ----------
    file_path_list : list[str]
        A list of full paths to the files
    """
    return [join(directory,f) for f in listdir(directory) if isfile(join(directory, f))]


def get_file_names_from_dir(directory):
    """
    Extracts every file from a directory with name without the extension

    Parameters
    ----------
    directory : str
         The path to the directory

    Return
    ----------
    file_path_list : list[str]
        A list of filenames of the files
    """
    return [get_filename_of_path(file) for file in load_files_from_directory(directory)]


def get_filename_of_path(path):
    """
    Gets the file name of a path without the file extension

    Parameters
    ----------
    path : str
         A path to file

    Return
    ----------
    file_name : str
        The file name
    """
    return splitext(basename(path))[0]


def divide_chunks(list, chunk_len):
    """
    Divide a list into chunks of a specific length

    Parameters
    ----------
    list : list
         A list of point of interest objects.
    chunk_len: int

    Return
    ----------
    chunk_list : list[list]
        A list of lists with the specified length
    """
    # looping till length l
    for i in range(0, len(list), chunk_len):
        yield list[i:i + chunk_len]

@contextmanager
def suppress_stdout():
    """
    Use to suppress stdout like prints or log infos
    """
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout


def find_cycle(value_list):
    """
    Find the first cycle in a list 

    Parameters
    ----------
    value_list: [float]
        The list where the cycle should be found

    Return
    ----------
    cycle : [float]
        A list that is the first cycle in the input list. If no cycle found, return None.
    """
    seen = set()
    cycle = []
    
    for value in value_list:
        if value in seen:
            cycle_start = cycle.index(value)
            # return the saved cycle if the same value appears a second time
            return cycle[cycle_start:]
        seen.add(value)
        cycle.append(value)
    # return None if no cycle was found
    return None
