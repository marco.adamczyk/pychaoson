import threading
import time
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import ipywidgets as widgets
import numpy as np
from IPython.display import display
import pychaoson.sensitivity_calibration as sensitivity_calibration
import pychaoson.config as config
import pychaoson.calculate_chaos as calculate_chaos

# set the used output for the plot and widgets
output = widgets.Output()


class PlotInfo:
    """
    Saves the data about the plot an implements methods that transpose values and distances from parameter space of the chaotic sytem
    to the plot space.
    """
    def __init__(self, plot_space, plot_param_space, plot_dims, plot_dim_names, im, axd, fig, dimension_data):
        # init the parameters that has to be stored of the plot
        self.plot_space = plot_space
        self.plot_param_space = plot_param_space
        self.im = im
        self.axd = axd
        self.dimension_data = dimension_data
        self.updated_img = False
        self.fig = fig
        self.plot_dims = plot_dims
        self.plot_dim_names = plot_dim_names

    def get_dim_count(self):
        return len(self.plot_dims)
    
    def transpose_point(self, space1, space2, param_point):
        # transpose the point on the x dimension by scaling the point in one dimension at a time
        transposed_point = []

        # compute the differnce between the min x and max x value in space 1
        param_space_x = space1[0]
        x_diff = np.linalg.norm(param_space_x[1] - param_space_x[0])

        # compute the difference between the min x and the point x in space 1
        x_point_diff = np.linalg.norm(param_point[0] - param_space_x[0])
        scaling = x_point_diff / x_diff
        param_space_2_x = space2[0]

        #computing the difference between the min x and max x value in space2
        x_diff = np.linalg.norm(param_space_2_x[1] - param_space_2_x[0])

        # scale the space2 x difference by the percentage of the distance in the space1 and add it to the x min of space2
        transposed_point.append(param_space_2_x[0] + scaling * x_diff)
        
        # do the same for the first dimension
        if len(param_point) != 1:
            param_space_y = space1[1]
            y_diff = np.linalg.norm(param_space_y[1] - param_space_y[0])
            y_point_diff = np.linalg.norm(param_point[1] - param_space_y[0])
            percentage = y_point_diff / y_diff

            param_space_2_y = space2[1]
            y_diff = np.linalg.norm(param_space_2_y[1] - param_space_2_y[0])
            transposed_point.append(param_space_2_y[0] + percentage * y_diff)
        return transposed_point

    def transpose_radius(self, space1, space2, radius):
        # get the transposed radius by selecting two points in the first space than transforming them and select
        if len(self.plot_dims) == 1:
            point = [space1[0][0]]
        else:
            point = [space1[0][0], space1[1][0]]
        # select two point from the first space and shift the second one by the space1 radius
        point_shifted = point.copy()
        point_shifted[0] += radius
        # transpose both points
        point_transposed = self.transpose_point(space1, space2, point)
        point_shifted_transposed = self.transpose_point(space1, space2, point_shifted)
        # get the distance between the transposed points
        return np.linalg.norm(point_transposed[0] - point_shifted_transposed[0])


class InteractiveParamspaceAnalysis:
    """
    This class creates an 2d heatmap plot for a chaotic system map and displays the start value, interesting ponts and the current played values.  Also
    two plots are created that display the time series of the generated output. First the init values of the output when played and the second a cyle in the ouptut if 
    one was found.
    By clicking on the heat map the start values of the chaotic parameters can be altered.
    """
    def __init__(self, pychaos, dimension_analysis_type,
                 points_per_dim=100,
                 time_series_len=5000, take_last=300):
        # save the choosen dimension analysis function to a array otherwise it cannot be called
        self.dimension_analysis_type = dimension_analysis_type
        # set the remaining passed arguments
        self.points_per_dim = points_per_dim
        self.time_series_len = time_series_len
        self.take_last = take_last
        self.pychaos = pychaos

        # assert that all maps are loaded
        for chaotic_map in self.pychaos.chaotic_systems_dict.values():
            calculate_chaos.dimension_analysis(self.pychaos, chaotic_map, chaotic_map.get_plot_dim(),
                                        self.points_per_dim, method=self.dimension_analysis_type, no_transform=True)

        # save the pick event id in order to check if a pick event is connected to the plot
        self.pick_event_id = None

        # stores the info of the plot
        self.plot_info = None 
        # init the plot
        with plt.ioff():
            fig, ax_dict = plt.subplot_mosaic([['heatmap', 'init plot'],
                                                ['heatmap', 'cycle plot']],
                                                figsize=(10, 5), layout="constrained")
        fig.suptitle('Interactive Analysis', fontsize=16)

        # set plot settings for the cycle plot
        ax_dict['cycle plot'].set_ylabel("Value")
        ax_dict['cycle plot'].set_xlabel("Time")
        ax_dict['cycle plot'].set_title("Repeating cycle(display max 100 points)")
        ax_dict['cycle plot'].axis(ymin=0,ymax=1.0)

        # set plot settings for the cycle plot
        ax_dict['init plot'].set_ylabel("Value")
        ax_dict['init plot'].set_xlabel("Time")
        ax_dict['init plot'].set_title("First values before circle (display max 100 points)")
        ax_dict['init plot'].axis(ymin=-0.5,ymax=1.5)
        self.fig = fig
        self.ax_dict = ax_dict

        #initialize the heatmap subplot by calculating the values to display first
        self.__init_heatmap__()

        # display the plot
        plt.show()

        # init widgets that enable reprocessing and changing the computed points per dimension
        self.__init_interactions__()

        # start a new thread in which a loop is updating the values of the interactive plot
        update_dimension_plot_thread = UpdateDimensionPlot(self.pychaos, self)
        update_dimension_plot_thread.start()

    def __init_heatmap__(self):
        fig = self.fig
        axd = self.ax_dict
        # get the selected chaotic system from api
        chaotic_system = self.pychaos.synth.chaotic_system
        # get the dimensions to plot from api
        plot_dims = chaotic_system.get_plot_dim()
        plot_dim_names = chaotic_system.get_plot_dim_names()

        param_space = chaotic_system.get_param_space()
        # compute the data to display as a heatmap
        dimension_data, _ = calculate_chaos.dimension_analysis(self.pychaos, chaotic_system, plot_dims,
                                               self.points_per_dim, method=self.dimension_analysis_type)

        # init plot depending it there are two dimensions to plot or only one
        if len(plot_dims) == 1:
            im = self.__plot_1d_heatmap__(axd['heatmap'], dimension_data, param_space, plot_dims)
        if len(plot_dims) == 2:
            im = self.__plot_2d_heatmap__(axd['heatmap'], dimension_data, param_space, plot_dims)
        if len(plot_dims) > 2:
            raise NotImplemented

        # set the parameter space and plot space depending of dimension to plot
        x_param_space = param_space[plot_dims[0]]
        if len(plot_dims) == 1:
            plot_param_space = [[x_param_space[1], x_param_space[2]]]
            plot_space = [list(axd['heatmap'].get_xlim())]
        else:
            x_param_space = param_space[plot_dims[0]]
            y_param_space = param_space[plot_dims[1]]
            plot_param_space = [[x_param_space[1], x_param_space[2]], [y_param_space[1], y_param_space[2]]]
            plot_space = [list(axd['heatmap'].get_xlim()), list(axd['heatmap'].get_ylim())]

        # set the new data to the object that stores all the plot data that is needed in the interactive plot loop
        if self.plot_info is None:
            # create a new object is its the first time this init function was called
            self.plot_info = PlotInfo(plot_space, plot_param_space, plot_dims, plot_dim_names, im, axd, fig, dimension_data)
        else:
            # update the object if the init method was already executed before
            self.plot_info.plot_space = plot_space
            self.plot_info.plot_param_space = plot_param_space
            self.plot_info.plot_dims = plot_dims
            self.plot_info.plot_dim_names = plot_dim_names
            self.plot_info.im = im
            self.plot_info.dimension_data = dimension_data

        # disconnect the pick event if one existed in the past
        if self.pick_event_id is not None:
            fig.canvas.mpl_disconnect(self.pick_event_id)

        # add new pick event
        self.__add_on_pick_event__()

    def __plot_2d_heatmap__(self, ax, data, param_space, dims):
        # swap axes to display the first dimension on the x axes
        data = np.swapaxes(data, 0, 1)

        # clean the axis and then show the heatmap with the 0,0 value being displayed at the bottom left
        ax.clear()
        im = ax.imshow(data, cmap='plasma', extent=[0, data.shape[0], 0, data.shape[1]], origin='lower', aspect="auto")

        # setting the labels so that the user knows which dimension is connected to which axis 
        # and make the scale on the axis match the parameter point at that point
        param = param_space[dims[0]]
        ax.set_xticks(ticks=[0, data.shape[0] - 1], labels=[param[1], param[2]])
        ax.set_xlabel("Dim: {}".format(dims[0]))
        param = param_space[dims[1]]
        ax.set_yticks(ticks=[0, data.shape[1] - 1], labels=[param[1], param[2]])
        ax.set_ylabel("Dim: {}".format(dims[1]))
        ax.set_aspect('equal')
        return im

    def __plot_1d_heatmap__(self, ax, data, param_space, dims):
        # expand the 1D data with equal values on a new axis that has the same elements as the 1D data.
        data = np.repeat(data[:,np.newaxis], len(data), axis=1)
        # swap axes to display the first dimension on the x axes
        data = np.swapaxes(data, 0, 1)
        # clean the axis and then show the heatmap with the 0,0 value being displayed at the bottom left
        ax.clear()
        im = ax.imshow(data, cmap="plasma", interpolation='nearest', origin='lower', aspect="auto")

        # setting the labels so that the user knows which dimension is connected to which axis 
        # and make the scale on the axis match the parameter point at that point
        ax.set_xticks(ticks=[0, data.shape[0] - 1], labels=[param_space[0][1], param_space[0][2]])
        ax.set_yticks([], [])
        ax.set_xlabel("Dim: {}".format(dims[0]))
        ax.set_ylabel(" ")
        ax.set_xlim()
        return im

    def __add_on_pick_event__(self):
        def on_pick(event):
            # get the plot points
            if not self.pychaos.synth.is_precomputing and not self.pychaos.update_synth:
                x = event.xdata
                y = event.ydata
                # assure that the selected point belongs to the heatmap plot
                if x is not None and y is not None and event.inaxes.get_title() == self.plot_info.axd['heatmap'].get_title():
                    # create a poin that dimension is determined by the plot dimension
                    if len(self.plot_info.plot_dims) == 1:
                        plot_point = [x]
                    else:
                        plot_point = [x, y]
                    # check if point lies inside the boundaries of the chaotic system parameters
                    transposed_point = self.plot_info.transpose_point(self.plot_info.plot_space,
                                                                    self.plot_info.plot_param_space, plot_point, )
                    self.pychaos.synth.set_chaos_start_params(transposed_point, self.plot_info.plot_dim_names)

        # let the trigger function above being executed with a button press
        self.pick_event_id = self.plot_info.fig.canvas.mpl_connect('button_press_event', on_pick)


    def __init_interactions__(self):
        global output

        # create the input for the defining the points for each dimension when recomputing the dimension info
        point_per_dim_ui = widgets.IntText(
            value=self.points_per_dim,
            description='Points per dim:',
            disabled=False
        )
        display(point_per_dim_ui, output)

        def point_per_dim_changed(change):
            self.points_per_dim = change['new']
        # add a function that will be triggered when the value of the input change
        # this new value is stored in a variable of the object
        point_per_dim_ui.observe(point_per_dim_changed, names='value')

        # create a widget button that triggers a recomputation of the dimension analysis 
        reprocess_button_ui = widgets.Button(
            description='Reprocess Image',
            disabled=False,
            button_style='',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me'
        )
        display(reprocess_button_ui, output)
        
        # add a function that will be executed when the widget reprocessing button is pressed
        def reprocess_button_clicked(b):
            with output:
                # reinit the heatmap of the plot
                self.__init_heatmap__()
                # redraw the plot
                self.plot_info.fig.canvas.draw_idle()
                
        reprocess_button_ui.on_click(reprocess_button_clicked)

class UpdateDimensionPlot(threading.Thread):
    """
    A thead class that executes the update loop for the plot to stay up-to-date with the synth values.
    """
    def __init__(self, pychaos, parameter_space_analysis):
        super().__init__()

        # set the api object and interactive plot object
        self.pychaos = pychaos
        self.parameter_space_analysis = parameter_space_analysis

        # init object variables
        self.x_scatter = []
        self.y_scatter = []
        self.color_scatter =[]
        self.size_scatter =[]
        self.box = None
        self.line_init = None
        self.line_init = None
        self.run_loop = True
    
    def run(self):
        """
        Starts the update loop.
        """
        try:
            # starting the new thread

            # draw on the line plots for the first time so that in the loop only a set on the line object is needed 
            line_length = 100
            line_values = np.empty(line_length)
            plot_info  = self.parameter_space_analysis.plot_info
            init_plot_line, = plot_info.axd['init plot'].plot(line_values)
            cycle_plot_line, = plot_info.axd['cycle plot'].plot(line_values)

            # init values that store results from the previous loop execution to check if an new update is necessary
            scat = None
            previous_x_scatter_values = None
            previous_y_scatter_values = None
            previous_param_values = None
            previous_selected_point = None
            previous_selected_point_radius = None

            # start loop
            while self.run_loop:
                # first always check if the chaotic system is updated 
                if self.pychaos.update_synth:
                    # it its true loop until it not updating anymore. Then reinit heatmap. This is necessary, because 
                    # when the values in the api start to change but the plot info is not updated, errors will occur
                    while self.pychaos.update_synth is not False:
                        time.sleep(1)
                    self.parameter_space_analysis.__init_heatmap__()

                # assign to local variable to ensure readability
                plot_info  = self.parameter_space_analysis.plot_info
                synth = self.pychaos.synth

                # reinit the object scatter values
                self.x_scatter = []
                self.y_scatter = []
                self.color_scatter =[]
                self.size_scatter =[]

                # flag indicated if a redraw of the plot should be executed in this loop iteration
                redraw = False

                # get the set start parameter values for the synth from the api
                start_param = synth.get_chaos_start_param(plot_info.plot_dims)
                
                #transpose this point from parameter space to plot space
                transposed_point = plot_info.transpose_point(plot_info.plot_param_space,
                                                                plot_info.plot_space, start_param)
                # add this start point to scatter
                self.__append_to_scatter__(transposed_point, 'white', 35)
                
                # if the synth is playing something add the current parameter value for the synth from the api and add it to the scatter
                if synth.playing:
                    current_points = synth.get_chaos_current_param(plot_info.plot_dims)
                    if current_points != []:
                        for current_point in current_points:
                            transposed_point = plot_info.transpose_point(plot_info.plot_param_space,
                                                                            plot_info.plot_space, current_point)
                            self.__append_to_scatter__(transposed_point, 'black', 5)
                
                # get all pois from api
                pois = synth.get_pois()

                # add a scatter point for every poi
                if pois:
                    for poi in pois:
                        poi_denormalized = sensitivity_calibration.denormalize_point(list(synth.chaotic_sliders_dict.values()), poi.poi)
                        poi_denormalized = [poi_denormalized[dim] for dim in plot_info.plot_dims]
                        transposed_point = plot_info.transpose_point(plot_info.plot_param_space,
                                                                        plot_info.plot_space, poi_denormalized)
                        self.__append_to_scatter__(transposed_point, 'gray', 15)

                # get the selected poi from api
                poi = synth.get_selected_poi()
                
                # add a box or a circle around that poi that shows its influence
                if poi is not None:
                    poi_denormalized = sensitivity_calibration.denormalize_point(list(synth.chaotic_sliders_dict.values()), poi.poi)
                    poi_denormalized = [poi_denormalized[dim] for dim in plot_info.plot_dims]
                    # only set if the point changed
                    if previous_selected_point != poi.poi or previous_selected_point_radius != poi.radius:
                        previous_selected_point = poi.poi
                        previous_selected_point_radius = poi.radius
                        # transpose the position of the point of interest
                        transposed_point = plot_info.transpose_point(plot_info.plot_param_space,
                                                                        plot_info.plot_space, poi_denormalized)
                        # transpose the radius of the influence
                        transposed_radius = plot_info.transpose_radius([[0,1], [0,1]], plot_info.plot_space, poi.radius)
                        # remove the box / circle if already exist
                        if self.box is not None:
                            self.box.remove()
                            self.box = None
                        self.__add_box__(transposed_point, transposed_radius, 'gray', 0.55)
                else:
                    if self.box is not None:
                        self.box.remove()
                        self.box = None

                # get the current parameter played by the synth
                current_parameter_values = synth.get_chaos_current_param()
                if (len(current_parameter_values) == 0):
                    current_parameter_values = synth.get_chaos_current_param_of_note(config.LOWEST_MIDI_NOTE)
                else:
                    current_parameter_values = current_parameter_values[0]
                # trying to update the cycle and init plot if values changed 
                if current_parameter_values != previous_param_values:
                    # get the cycle values list from api
                    cycles_dict = synth.value_manager.cycle_dict
                    if str(current_parameter_values) in cycles_dict.keys():
                        cycle_values = cycles_dict[str(current_parameter_values)]
                        
                        # init a line with nan values with the length defined during the plot init outside the loop
                        cycle_line = np.full(line_length, np.nan)

                        # check if a cycle exists. If true, overwrite the line.
                        if cycle_values is not None:
                            cycle_values = cycle_values.copy()
                            if len(cycle_values) == 1:
                                # if the cycle consists of only one value then double it to make it visible in the plot
                                cycle_values = cycle_values + cycle_values
                            if len(cycle_values) > line_length:
                                cycle_values = cycle_values[:line_length]
                            cycle_line[0:len(cycle_values)] = cycle_values
                        cycle_plot_line.set_ydata(cycle_line)
                        # sleep necessary due to asynchronous execution
                    # get the init values list from api
                    init_dict = synth.value_manager.init_dict
                    if str(current_parameter_values) in init_dict.keys():   
                        init_values = init_dict[str(current_parameter_values)]

                        # init a line with nan values with the length defined during the plot init outside the loop
                        init_line = np.full(line_length, np.nan)
                        if len(init_values) == 1:
                            # if the init value before going into a loop consists of only one value then double it to make it visible in the plot
                            init_values = init_values + init_values
                        init_line[0:len(init_values)] = init_values

                        init_plot_line.set_ydata(init_line)
                        # sleep necessary due to asynchronous execution
                    # set the new values as the previous to execute a change check in the next iteration
                    previous_param_values = current_parameter_values
                    previous_param_values = current_parameter_values
                # check if properties of the plot change. Only then it should be redrawn
                if plot_info.updated_img:
                    # set the new dimension data to the plot
                    plot_info.im.set_data(plot_info.dimension_data)
                    plot_info.updated_img = False
                if previous_y_scatter_values != self.y_scatter or previous_x_scatter_values != self.x_scatter:
                    if scat is not None:
                        scat.remove()
                    # pot the scatter values
                    scat = plot_info.axd['heatmap'].scatter(self.x_scatter, self.y_scatter, c=self.color_scatter, s=self.size_scatter, alpha=0.55)
                    # sleep necessary due to asynchronous execution
                    redraw = True
                    # set the new values as the previous to execute a change check in the next iteration
                    previous_y_scatter_values = self.y_scatter
                    previous_x_scatter_values = self.x_scatter
                if redraw:
                    # Redraw the plot if the figure and canvas are available
                    plot_info.fig.canvas.draw_idle()
                    plot_info.fig.canvas.flush_events()
                # sleep necessary due to asynchronous execution
                time.sleep(0.25)
        except Exception as e:
            print(type(e))
            print(e)

    def __add_box__(self, transposed_point, transposed_radius, color, alpha):
        plot_info  = self.parameter_space_analysis.plot_info
        facecolor = 'none'
        if len(transposed_point) == 1:
            # for 1d plot a rectangle is displayed that has the extend of the radius of the poi in the x dimension and the whole range on the y axis because the y axis
            # is only a duplicate of the 1D values
            self.box = Rectangle((transposed_point[0] - transposed_radius, plot_info.plot_space[0][0]),transposed_radius * 2, plot_info.plot_space[0][1],linewidth=1,edgecolor=color,facecolor=facecolor)
            # Add the patch to the axes
            plot_info.axd['heatmap'].add_patch(self.box)
        else:
            # for 3d a circle is needed with the expected radius
            self.box = plt.Circle((transposed_point[0], transposed_point[1]), transposed_radius, color=color)
            plot_info.axd['heatmap'].add_artist(self.box)
            self.box.set_clip_box(plot_info.axd['heatmap'].bbox)
            self.box.set_edgecolor(color)  
            self.box.set_facecolor(facecolor)
            self.box.set_alpha(alpha)

    def __append_to_scatter__(self, plot_point, color, size):
        # appends a scatter points info to a list to later be displayed at once
        plot_info  = self.parameter_space_analysis.plot_info
        # set the point of the scatter in the image
        self.x_scatter.append(plot_point[0])
        # if the plot dim is set to one always set the y value to be in the middle of the y axis
        if len(plot_point) > 1:
            self.y_scatter.append(plot_point[1])
        else:
            self.y_scatter.append(np.average(plot_info.plot_space[0]))
        # append color and size of the plot
        self.color_scatter.append(color)
        self.size_scatter.append(size)
