from IPython.core.display_functions import display
import ipywidgets as widgets
import pychaoson.sc_helper as sc_helper
import pychaoson.sensitivity_calibration as sensitivity_calibration
import pychaoson.sliders as sliders


def control_mappings(pychaos):
    """
    Create widgets are controlling the synth behaviour from a jupyter notebook cell. These widgets are controlling the mapping 
    of synth variables to the roli control.

    Parameters
    ----------
    pychaos : Pychaoson
        A pychaoson instance 

    Returns
    -------
    vbox: ipywidgets.VBox
        A container that stores the dropdowns with the selectable synth variables for every roli dimension.
    """
    # get the possible dropdown values for the roli mapping dropdowns
    slider_names = list(pychaos.synth.all_sliders_dict.keys()) + ['No Mapping']
    roli_control = pychaos.roli_control

    # init the widget for displaying the roli x mapping
    x_mapping_ui = widgets.Dropdown(
        options=slider_names,
        value=None,
        description='X Mapping:',
        disabled=False
    )
    def change_x_mapping(change):
        slider_name = change['new']
        # set or unset the connected slider to the roli dimension
        if slider_name == 'No Mapping':
            roli_control.unset_roli_control('x')
        else:
            roli_control.set_roli_control('x', slider_name)
        
    # add a function that is executed when the x mapping vlaue of the dropdown changed
    x_mapping_ui.observe(change_x_mapping, names='value')
    
    # set the init vlaue depending of the currently connected slider
    if roli_control.get_roli_control_slider('x') is None:
        x_mapping_ui.value = 'No Mapping'
    else:
        x_mapping_ui.value = roli_control.get_roli_control_slider('x').get_name()

    y_mapping_ui = widgets.Dropdown(
        options=slider_names,
        value=None,
        description='Y Mapping:',
        disabled=False
    )
    def change_y_mapping(change):
        slider_name = change['new']
        # set or unset the connected slider to the roli dimension
        if slider_name == 'No Mapping':
            roli_control.unset_roli_control('y')
        else:
            roli_control.set_roli_control('y', slider_name)
        
    # add a function that is executed when the x mapping vlaue of the dropdown changed
    y_mapping_ui.observe(change_y_mapping, names='value')

    # set the init vlaue depending of the currently connected slider
    if roli_control.get_roli_control_slider('y') is None:
        y_mapping_ui.value = 'No Mapping'
    else:
        y_mapping_ui.value = roli_control.get_roli_control_slider('y').get_name()
        

    z_mapping_ui = widgets.Dropdown(
        options=slider_names,
        value=None,
        description='Z Mapping:',
        disabled=False
    )
    def change_z_mapping(change):
        # convert the string to a list of integers
        slider_name = change['new']
        # set or unset the connected slider to the roli dimension
        if slider_name == 'No Mapping':
            roli_control.unset_roli_control('z')
        else:
            roli_control.set_roli_control('z', slider_name)
        
    # add a function that is executed when the x mapping vlaue of the dropdown changed
    z_mapping_ui.observe(change_z_mapping, names='value')

    # set the init vlaue depending of the currently connected slider
    if roli_control.get_roli_control_slider('z') is None:
        z_mapping_ui.value = 'No Mapping'
    else:
        z_mapping_ui.value = roli_control.get_roli_control_slider('z').get_name()
        

    # adding the dropdown widgets to a vertical box for better layouting
    vbox = widgets.VBox([
        x_mapping_ui,
        y_mapping_ui,
        z_mapping_ui,
    ])
    return vbox

def control_synth(pychaos, called_by_full_control=False):
    """
    Create widgets are controlling the synth behaviour from a jupyter notebook cell. These widgets are controling the synth synthesis type, 
    used samples and used chaotic system.

    Parameters
    ----------
    pychaos : Pychaoson
        A pychaoson instance 
    called_by_full_control: bool, false
        If true tries to reload the full control gui

    Returns
    -------
    vbox: ipywidgets.VBox
        A container that stores the select widgets with the possible synth parameters to choose
    """
    # get the samle and chaotic system data from api
    sample_name_list = pychaos.samples_dict.keys()
    current_sample_name = pychaos.synth.current_sample_name
    chaotic_system_name_list = pychaos.chaotic_systems_dict.keys()
    current_chaotic_system_name = pychaos.synth.chaotic_system.get_name()
    selected_sample_name= pychaos.synth.current_sample_name
    if selected_sample_name is None:
        selected_sample_name = 'No sample'
    selected_sample_text =  widgets.Text(
        value=selected_sample_name,
        placeholder='Type something',
        description='Selected:',
        disabled=True   
    )   
    if current_sample_name is None:
        current_sample_name = list(sample_name_list)[0]
    # create drowdown widget for the samples
    sample_names_dropdown = widgets.Select(
        options=sample_name_list,
        value=current_sample_name,
        description='Samples:',
        disabled=False
    )

    selected_map_text =  widgets.Text(
        value=current_chaotic_system_name,
        placeholder='Type something',
        description='Selected:',
        disabled=True   
    )   
    # create drowdown widget for the chaotic maps
    chaotic_system_names_dropdown = widgets.Select(
        options=chaotic_system_name_list,
        value=current_chaotic_system_name,
        description='Maps:',
        disabled=False
    )
    selected_playtype_text = widgets.Text(
        value=pychaos.synth.playtype,
        placeholder='Type something',
        description='Selected:',
        disabled=True   
    )   
    # create drowdown widget for the samples
    playtype_dropdown = widgets.Select(
        options=pychaos.playtype_names,
        description="Gen Type:",
        value=pychaos.synth.playtype
    )
    # add a button that play the currently selected sample for granular synthesis
    if pychaos.synth.playtype == 'Granular':
        play_sample__button_ui = widgets.Button(
            description='Play Selected Sample',
            disabled=False,
            button_style='',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me'
        )
        

        def play_sample_button_clicked(b):
            # call the api to play the selected sample
            if pychaos.synth.playtype == 'Granular':
                sc_helper.play_buffer(pychaos.sc, pychaos.synth.bufnum_dict[69])

        # playing sample is executed on button click
        play_sample__button_ui.on_click(play_sample_button_clicked)


    # add button to update the api to the selected values in the gui
    update_button_ui = widgets.Button(
        description='Update',
        disabled=False,
        button_style='',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me'
    )

    def update_button_clicked(b):
        if b:
            if playtype_dropdown.value == 'Granular':
                sample_name = sample_names_dropdown.value
            else:
                sample_name = None
            chaotic_system_name = chaotic_system_names_dropdown.value
            # call the api method to reset the synthesis to the selected values in the widgets
            pychaos.start_synth(chaotic_system_name, playtype_dropdown.value, sample_name)
            # reset the other gui elements
            if called_by_full_control:
                full_control(pychaos, reload=True, selected_tab_index=1)
            else: 
                display(control_synth(pychaos))
            tab.layout.visibility = 'visible'

    # update synth is executed on button click
    update_button_ui.on_click(update_button_clicked)

    # add the select widgets in a horizontal box to save space
    hbox = widgets.HBox([
                        widgets.VBox([selected_map_text, chaotic_system_names_dropdown]),
                        widgets.VBox([selected_playtype_text, playtype_dropdown]),
                        widgets.VBox([selected_sample_text, sample_names_dropdown])
                        ])
    if pychaos.synth.playtype == 'Granular':
        vbox = widgets.VBox(
        [hbox,
            play_sample__button_ui,
            update_button_ui])
    else:
        vbox = widgets.VBox(
        [hbox,
         update_button_ui])
    # then add the buttons in a vbox below
    return vbox

selected_poi = None

def control_poi(pychaos):
    """
    Create widgets are controlling the synth behaviour from a jupyter notebook cell. These widgets are controling the points of interests in a synth
    that are wraping the parameter space of the chaotic system.

    Parameters
    ----------
    pychaos : Pychaoson
        A pychaoson instance 

    Returns
    -------
    vbox: ipywidgets.VBox
        A widget container that contains buttons for adding, removing point of interest and a select box that visualizes all available point of interests
    """
    # add a widget button that should add a new point of interest when selected
    add_poi_button_ui = widgets.Button(
        description='Add POI',
        disabled=False,
        button_style='',
        tooltip='Click me'
    )

    normalized_dict = dict()

    
    def update_poi_widget_list():
        # get all point of interest from the api
        current_pois = pychaos.synth.get_pois()
        nonlocal normalized_dict
        normalized_dict = dict()
        # for every point of interest write it to a dictionary that stores the normalized values as values and the denormalized values as keys
        chaos_slider_list = []
        for key in pychaos.synth.chaotic_sliders_dict.keys():
            chaos_slider_list.append(pychaos.synth.chaotic_sliders_dict[key])
        for current_poi in current_pois:
            denormalized_point_str = str(sensitivity_calibration.denormalize_point(chaos_slider_list, current_poi.poi))
            normalized_dict[denormalized_point_str] = current_poi.poi
        # set the dict as the options variable in the select point of interest widget
        select_ui.options = normalized_dict.keys()
        
    def add_poi_button_clicked(b):
        # add point to poi in the parameter manager
        start_point = pychaos.synth.get_chaos_start_param()
        pychaos.synth.add_poi(start_point)
        # update select widget list
        update_poi_widget_list()

    # execution the add point of interest method when the corresponding button is clicked
    add_poi_button_ui.on_click(add_poi_button_clicked)

    # initialize a widget button to remove a point of interest
    remove_poi_button_ui = widgets.Button(
        description='Remove POI',
        disabled=False,
        button_style='',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me'
    )

    def remove_poi_button_clicked(b):
        global selected_poi
        if selected_poi != None:
            # call the api to remove this point of interest
            pychaos.synth.remove_poi(selected_poi)
            pychaos.synth.set_selected_poi(None)

        # update select widget list
        update_poi_widget_list()

    # connect the remove selected point of interest method to the widget button
    remove_poi_button_ui.on_click(remove_poi_button_clicked)

    # init the select widget for the existing points of interest
    select_ui = widgets.Select(
        options=[],
        description='POIs:',
        disabled=False
    )
    update_poi_widget_list()

    def poi_selection_changed(change):
        global selected_poi

        selected_poi = change['new']
        if selected_poi in normalized_dict.keys():
            # set this poi to the currently selected poi
            for poi in pychaos.synth.get_pois():
                if poi.poi == normalized_dict[selected_poi]:
                    selected_poi = poi
                    pychaos.synth.set_selected_poi(poi)
                    # reset the radius and exp inputs
                    selected_poi_radius_ui.layout.visibility = None
                    selected_poi_exp_ui.layout.visibility = None
                    selected_poi_radius_ui.value = selected_poi.radius
                    selected_poi_exp_ui.value = selected_poi.exp
        else:
            # if the select point is not in keys then remove the curretnlu select poi
            pychaos.synth.remove_poi(selected_poi)
            # also hide radius and exp inputs
            selected_poi_radius_ui.layout.visibility = 'hidden'
            selected_poi_exp_ui.layout.visibility = 'hidden'

    # call the selection change function when the select widget state change
    select_ui.observe(poi_selection_changed, names='value')

    # init widget for the exp input
    selected_poi_exp_ui = widgets.IntText(
        value=None,
        description='POI Exp:',
        disabled=False
    )
    # make widget hidden at first
    selected_poi_exp_ui.layout.visibility = 'hidden'

    def selected_poi_exp_changed(change):
        global selected_poi
        # set the selected poi exp to the new exp value
        selected_poi.exp = change['new']

    # call the selection change function when the exp input widget state change
    selected_poi_exp_ui.observe(selected_poi_exp_changed, names='value')

    # init widget for the radius input
    selected_poi_radius_ui = widgets.FloatText(
        value=None,
        description='POI Radius:',
        disabled=False
    )
    # make widget hidden at first
    selected_poi_radius_ui.layout.visibility = 'hidden'

    def selected_poi_radius_changed(change):
        global selected_poi
        selected_poi.radius = change['new']

    # call the selection change function when the exp input widget state change
    selected_poi_radius_ui.observe(selected_poi_radius_changed, names='value')
    
    #add to vbox container for layouting
    vbox = widgets.VBox(
        [add_poi_button_ui,
         select_ui,
         selected_poi_exp_ui,
         selected_poi_radius_ui,
         remove_poi_button_ui
         ]
    )
    return vbox


slider_uis = []
sensitivity_uis = []
reverse_uis = []
def control_sliders(pychaos):
    """
    Create widgets are controlling the synth behaviour from a jupyter notebook cell. These widgets are controling parameter of the synth directly with sliders.
    Also the behavior of the parameters can be set when mapped to a roli control.
    Parameters
    ----------
    pychaos : Pychaoson
        A pychaoson instance 

    Returns
    -------
    vbox: ipywidgets.VBox
        A widget container that contains sliders and dropdowns.
    """
    # access the global widgets list
    global slider_uis
    global sensitivity_uis
    global reverse_ui

    # set the widget list to empty
    slider_uis = []
    sensitivity_uis = []
    reverse_uis = []


    # get the current sliders from api
    all_sliders_dict = pychaos.synth.all_sliders_dict
    all_sliders = []
    for key in all_sliders_dict.keys():
        all_sliders.append(all_sliders_dict[key])

    def slider_value_changed(change):
        # get the index in the widget list
        index = slider_uis.index(change.owner)
        # this index corresponds with the index of the sliders in the slider list
        # then set the change value as start value of the slider
        slider_name = all_sliders[index].get_name()
        pychaos.synth.set_start_param(change['new'], slider_name)

    def sensitivity_ui_value_changed(change):
        # get the index in the widget list
        index = sensitivity_uis.index(change.owner)
        # this index corresponds with the index of the sliders in the slider list
        # then set the change value as linear sensetivity of the slider
        slider_name = all_sliders[index].get_name()
        pychaos.synth.set_sensitivity_param(change['new'], slider_name)


    def reverse_ui_value_changed(change):
        # get the index in the widget list
        index = reverse_uis.index(change.owner)
        reverse_bool = False
        if change['new'] == 'Yes':
            reverse_bool = True

        slider_name = all_sliders[index].get_name()
        pychaos.synth.set_reverse_param(reverse_bool, slider_name)

    v_box_list = []
    titles = []
    # init the widgets for every slider
    for i in range(len(all_sliders)):
        #add a float slider for the value of the slider
        slider_ui = widgets.FloatSlider(
            value=all_sliders[i].get_start_value(),
            min=all_sliders[i].get_min_value(),
            max=all_sliders[i].get_max_value(),
            step=(all_sliders[i].get_max_value() - all_sliders[i].get_min_value()) / 50,
            description= all_sliders[i].get_name() + ":",
            disabled=False,
            continuous_update=False,
            orientation='horizontal',
            readout=True,
            readout_format='.1f',
        )
        slider_uis.append(slider_ui)

        # call the slider value change method when the widget changes it state
        slider_uis[i].observe(slider_value_changed, names="value")
        
        #add a float slider for the sensitivity of the slider
        sensitivity_ui = widgets.FloatSlider(
            value=all_sliders[i].get_linear_sensitivity(),
            min=0,
            max=1,
            step=0.05,
            description= "Sensitivity:",
            disabled=False,
            continuous_update=False,
            orientation='horizontal',
            readout=True,
            readout_format='.1f',
        )
        sensitivity_uis.append(sensitivity_ui)

        # call the sensitivity value change method when the widget changes it state
        sensitivity_uis[i].observe(sensitivity_ui_value_changed, names="value")

        # transform the boolean reverse value to be displayed as a string
        reverse_value = 'Yes'
        if not all_sliders[i].reverse:
            reverse_value = 'No'
        # init a dropdown widget for choosing if the slider should work reversed
        reverse_ui = widgets.Dropdown(
            options=["Yes", "No"],
            description="Reverse:",
            value=reverse_value
        )
        reverse_uis.append(reverse_ui)

        # call the reverse value change method when the widget changes it state
        reverse_uis[i].observe(reverse_ui_value_changed)
        
        # create a v box container to store all the widget belonging to one slider
        titles.append(all_sliders[i].get_name())
        vbox = widgets.VBox(
            [slider_ui,
             sensitivity_ui,
             reverse_ui]
        )
        # add this v box to a list of vbox containing the widgets of the slider
        v_box_list.append(vbox)
    # init an accordion widget container that stores the v box lists with the slider widgets
    accordion = widgets.Accordion(children=v_box_list, titles=titles)
    return accordion

selected_patch = None
def control_patches(pychaos, called_by_full_control=False):
    """
    Create widgets are controlling the synth behaviour from a jupyter notebook cell. These widgets are enabling the user to create patches, load patched and delete patches.
    These patches are displayed in a select box.

    Parameters
    ----------
    pychaos : Pychaoson
        A pychaoson instance 
    called_by_full_control: bool, optional
        If true tries to reload the full control gui

    Returns
    -------
    vbox: ipywidgets.VBox
        A widget container that contains buttons and a select box
    """
    # get the globally stored patch as name
    global selected_patch

    # get all found patches in the api
    patch_names = pychaos.get_available_patches()

    # init the widget that displays all the patches available
    patches_ui = widgets.Select(
        options=patch_names,
        value=selected_patch,
        description='Patches:',
        disabled=False
    )    
    def selected_patch_changed(change):
        # add the selected patch in the widget to the selected patch global variable
        global selected_patch
        patch_name = change['new']
        selected_patch = patch_name

    # call method that set the selected patch when selection of patches changed
    patches_ui.observe(selected_patch_changed, names='value')

    # widget button to load the selected path
    load_patch_ui = widgets.Button(
        description='Load patch',
        disabled=False,
        button_style='',
        tooltip='Click me'
    )

    def load_patch_button_clicked(b):
        if selected_patch is not None:
            # load a patch in the api
            pychaos.load_patch(selected_patch)
            # reload the control gui
            if called_by_full_control:
                full_control(pychaos, True)

    # execute the method for loading of a patch when click on the button widget
    load_patch_ui.on_click(load_patch_button_clicked)

    # init a widget button for deleting the selected patch
    delete_patch_ui = widgets.Button(
        description='Delete patch',
        disabled=False,
        button_style='',
        tooltip='Click me'
    )
    def delete_patch_button_clicked(b):
        if selected_patch is not None:
            # execute the delete patch method in the ui and update the available patches
            pychaos.delete_patch(selected_patch)
            patches_ui.options=pychaos.get_available_patches()

    # connect the delete patch method with the button
    delete_patch_ui.on_click(delete_patch_button_clicked)

    # init a widget where the user can write its own patch name
    new_patch_name_ui =widgets.Text(
        value='',
        placeholder='New patch name',
        description='',
        disabled=False
    )
    # init a button widget for adding a patch
    save_new_patch_ui = widgets.Button(
        description='Add patch',
        disabled=False,
        button_style='',
        tooltip='Click me'
    )
    def save_new_patch_clicked(b):
        nonlocal patch_names
        if new_patch_name_ui.value not in patch_names and new_patch_name_ui.value != '':
            # call the api method for saving a new patch with the name in the input text widget
            pychaos.save_patch(new_patch_name_ui.value)
            # relaoding the list of available patches
            patches_ui.options=pychaos.get_available_patches()

    # connect the save new patch method with the button   
    save_new_patch_ui.on_click(save_new_patch_clicked)

    # add all widgets to a vbox for layouting
    vbox = widgets.VBox(
            [patches_ui,
             load_patch_ui,
             delete_patch_ui,
             new_patch_name_ui,
             save_new_patch_ui]
        )
    return vbox
    
output = widgets.Output()
tab = None

def full_control(pychaos, reload=False, selected_tab_index=0):
    """
    Creates and displays a tabs with all the control available for the synth and roli control.

    Parameters
    ----------
    pychaos : Pychaoson
        A pychaoson instance 
    reload: bool, optional
        If true the tabs is created from scratch, if false an already existing tab widget is modified
    selected_tab_index: int, optional
        Selects with tab should be displayed when initializing
    """
    # access the global widgets that should not be reset when calling this method again
    global output
    global tab
    output.clear_output()
    # check if the widgets should be inititalized or only reset  
    if reload:
        children = tab.children[1:]
        # the patches never have to be reloaded
        tab.children = (tab.children[0], )
        for child in children:
            # close the old child values
            child.close
        # reset the tab children with new widgets
        tab.children = tab.children + (control_synth(pychaos, True), control_mappings(pychaos), control_sliders(pychaos), control_poi(pychaos))
        tab.titles = ('Patches', 'Synth Select','Roli Mappings', 'Parameter Control', 'Sensitivity Calibration')
        
        # set the selected tab
        tab.selected_index = selected_tab_index
        display(tab, output)
    else:
        tab = widgets.Tab()
        # completely initialize the children element of the tab
        tab.children = [control_patches(pychaos, True), control_synth(pychaos, True), control_mappings(pychaos), control_sliders(pychaos), control_poi(pychaos)]
        tab.titles = ('Patches', 'Synth Select','Roli Mappings', 'Parameter Control', 'Sensitivity Calibration')
        tab.selected_index = selected_tab_index
        display(tab, output)
