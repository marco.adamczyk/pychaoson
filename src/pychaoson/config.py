import os

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.join(CURRENT_DIR, "../../data")
SAMPLES_DIR = os.path.join(CURRENT_DIR, "../../samples")
SAMPLE_DATA_DIR = os.path.join(DATA_DIR, "sample_data")
RECORD_DATA_DIR = os.path.join(DATA_DIR, "record_data")
PATCH_DATA_DIR = os.path.join(DATA_DIR, "patch_data")

SCRIPTS_DIR = os.path.join(CURRENT_DIR, "../../scripts")

LOWEST_MIDI_NOTE = 12
HIGHEST_MIDI_NOTE = 108

def get_midi_note_list():
    """
    The all midi notes that should be considered.

    Returns
    -------
    pychaoson: [int]
        The midi note list
    """
    return list(range(LOWEST_MIDI_NOTE, HIGHEST_MIDI_NOTE + 1 ))