import pychaoson.sliders as sliders
import itertools
from sc3nb import Buffer
import mido
import threading
import sc3nb as scn
from datetime import datetime
import numpy as np


def get_midi_input_list():
    return mido.get_input_names()


class RoliMidiProcessing():
    """
    The object for the midi connection to the roli seabord and triggering synth 
    action base on live midi data and set connection to sliders.
    """
    def __init__(self, synth, port_name):
        """
        Initalizes the connection to the roli device.

        Parameters
        ----------
        synth : Synth
            The synth object that controls synthesis
        port_name : str
            The name of the roli device port. Input device ports can be checked with roli_control.get_midi_input_list()
        """
        self.current_attached_slider_names = []
        self.synth = synth
        self.port_name = port_name
        self.midi_processing_thread = None
        # define object variables
        self.channelNoteArray = [-1] * 17
        self.inport = None
        self.try_midi_open_times = 2
        self.roli_x_settings = RoliSettings([-300, 300], True)
        self.roli_x_start_value = 0
        self.roli_y_settings = RoliSettings([7, 123], True)
        self.roli_y_start_value = 0
        self.roli_z_settings = RoliSettings([0, 127], False)
        self.roli_z_start_value = 0
        self.roli_control_dims = ['x', 'y', 'z']
        # try connecting to midi port
        self.__connect_to_midi__()

    def run_midi_processing(self):
        """
        Start the control loop where midi data is collected and the synth controled. Is executed in another thread.
        """
        self.midi_processing_thread = threading.Thread(target=self.__attach_callback__)
        self.midi_processing_thread.start()
        return

    def stop_midi_processing(self):
        """
        Stop the midi callback loop and join thread.
        """
        if self.midi_processing_thread is not None:
            if self.midi_processing_thread.is_alive():
                self.__remove_callback__()
                self.midi_processing_thread.join()
    
    def get_roli_control_dims(self):
        """
        Return the roli controlable dimensions. This dims can be used to attach a parameter to that dimension.

        Returns
        -------
        roli_control_dims: [str]
            A list of the roli control dimensions.
        """
        return self.roli_control_dims

    def unset_all_roli_control(self):
        """
        Remove attached sliders for all roli dimensions.
        """
        for control_dim in self.roli_control_dims:
            self.unset_roli_control(control_dim)

    def unset_roli_control(self, control_dim):
        """
        Remove attached slider for specific roli dimension

        Parameters
        ----------
        control_dim : str
            The roli control dimension as a string
        """
        if control_dim == self.roli_control_dims[0]:
            if self.roli_x_settings.attached_slider != None:
                self.roli_x_settings.attached_slider.set_attached(False)
                self.current_attached_slider_names.remove(self.roli_x_settings.attached_slider.get_name())
                self.roli_x_settings.attached_slider = None
        if control_dim == self.roli_control_dims[1]: 
            if self.roli_y_settings.attached_slider != None:
                self.roli_y_settings.attached_slider.set_attached(False)
                self.current_attached_slider_names = [slider_name for slider_name in self.current_attached_slider_names.copy() if slider_name != self.roli_y_settings.attached_slider.get_name()]
                print(self.current_attached_slider_names)
                self.roli_y_settings.attached_slider = None
        if control_dim == self.roli_control_dims[2]:
            if self.roli_z_settings.attached_slider != None:
                self.roli_z_settings.attached_slider.set_attached(False)
                self.current_attached_slider_names.remove(self.roli_z_settings.attached_slider.get_name())
                self.roli_z_settings.attached_slider = None 

    def get_roli_control_slider(self, control_dim):
        """
        Get the slider that is connected to the roli control dimension. If its no slider is connected,
        None is returned.

        Parameters
        ----------
        control_dim : str
            The roli control dimension as a string

        Returns
        -------
        slider: Slider
            The attached slider object that holds the information for synthesis
        """
        if control_dim == self.roli_control_dims[0]:
            return self.roli_z_settings.attached_slider
        if control_dim == self.roli_control_dims[1]: 
            return self.roli_y_settings.attached_slider
        if control_dim == self.roli_control_dims[2]:
            return self.roli_z_settings.attached_slider
        raise Exception("No such slider dimension")
    
    def set_roli_control(self, control_dim, slider_name):
        """
        Get the slider that is connected to the roli control dimension. If its no slider is connected,
        None is returned.

        Parameters
        ----------
        control_dim : str
            The roli control dimension as a string
        slider_name : str
            The name of the slider object that should be attached to the roli dimension
        """
        slider = self.synth.all_sliders_dict[slider_name]
        if control_dim == self.roli_control_dims[0]:
            roli_settings = self.roli_x_settings
        elif control_dim == self.roli_control_dims[1]: 
            roli_settings = self.roli_y_settings
        elif control_dim == self.roli_control_dims[2]:
            roli_settings = self.roli_z_settings
        else:
            raise Exception("No such slider dimension")
        print('Exec')
        if slider_name not in self.current_attached_slider_names:
            print('Inside')
            self.current_attached_slider_names.append(slider_name)
            roli_settings.enabled = False
            roli_settings.attached_slider = slider
            self.current_attached_slider_names.append(slider_name)
            slider.set_attached(True)
            slider.set_centered(roli_settings.centered_values)
            value_range = roli_settings.midi_value_range
            value_count = len(list(range(value_range[0], value_range[1], 1)))
            slider.set_linspace_value_count(value_count)
            slider.compute_linspace()
            if type(slider) == sliders.SlidableParameter:
                self.synth.__precompute_chaos_output__()
            roli_settings.enabled = True

    def get_mapping_dict(self):
        """
        A dict of the attached sliders for every roli dimension.
        Is used for storing the current state

        Return
        ----------
        mapping_dict : dict
            The keys are the roli dimension names and the values are the attached slider names.
              If no slider is attached at dimension 'none' is attached
        """
        mapping_dict = dict()
        if self.roli_x_settings.attached_slider is None:
            mapping_dict['x'] = None
        else:
            mapping_dict['x'] = self.roli_x_settings.attached_slider.get_name()
        
        if self.roli_y_settings.attached_slider is None:
            mapping_dict['y'] = None
        else:
            mapping_dict['y'] = self.roli_y_settings.attached_slider.get_name()
        
        if self.roli_z_settings.attached_slider is None:    
            mapping_dict['z'] = None
        else:
            mapping_dict['z'] = self.roli_z_settings.attached_slider.get_name()
        return mapping_dict
   
    def stop(self):
        """
        Stops midi processing and close midi port
        """
        # close midi connection
        if self.inport is not None:
            self.stop_midi_processing()
            self.inport.close()

    def set_synth(self, synth):
        """
        Sets a new synth to be controlled by the midi control

        Parameters
        ----------
        synth : Synth
            The synth control object

        Returns
        -------
        pychaoson: Pychaoson
            SuperCollider Interface class.
        """
        self.synth = synth

    def __connect_to_midi__(self):
        for i in range(self.try_midi_open_times):
            try:
                self.inport = mido.open_input(self.port_name)
            except:
                if i < self.try_midi_open_times - 1:
                    print("Could not connect... Try again...")
                else:
                    raise Exception("Could not connect to midi")
        print("Connected...")

    def __midi_roli_callback__(self, message):
        try:
            # get the necessary variables
            message_dict = message.dict()
            message_type = message_dict['type']
            channel = message_dict['channel']
            # midi note off event
            if message_type == 'note_off':
                note = message_dict['note']
                self.synth.release_note(note)
            if not self.synth.is_precomputing:
                # midi note on event
                if message_type == 'note_on':
                    note = message_dict['note']
                    self.channelNoteArray[channel] = note
                    self.synth.play_note(note)

                # midi roli y parameter change event
                elif message_type == 'control_change':
                    note = self.channelNoteArray[channel]
                    if self.roli_y_settings.attached_slider is not None and self.roli_y_settings.enabled:
                        note = self.channelNoteArray[channel]
                        if note in self.synth.synth_dict.keys():
                            if not self.synth.synth_dict[note].freed:
                                position = message_dict['value']
                                self.__set_parameters__(self.roli_y_settings, note, position)

                # midi roli z parameter change event
                elif message_type == 'aftertouch':
                    if self.roli_z_settings.attached_slider is not None and self.roli_z_settings.enabled:
                        note = self.channelNoteArray[channel]
                        if note in self.synth.synth_dict.keys():
                            if not self.synth.synth_dict[note].freed:
                                pressure = message_dict['value']
                                self.__set_parameters__(self.roli_z_settings, note, pressure)

                # midi roli x parameter change event
                elif message_type == 'pitchwheel':
                    if self.roli_x_settings.attached_slider is not None and self.roli_x_settings.enabled:
                        # get midi channel to define the note and synth
                        note = self.channelNoteArray[channel]
                        if note in self.synth.synth_dict.keys():
                            if not self.synth.synth_dict[note].freed:
                                # define the parameter value of the x value
                                # limit the value to the set value range
                                pitch = message_dict['pitch']
                                # restrict the pitch values to the predefined range
                                self.__set_parameters__(self.roli_x_settings, note, pitch)
        except Exception as e:
            print(type(e))
            print(e)
        
    def __set_parameters__(self, roli_settings, note, midi_value):
        slider = roli_settings.attached_slider
        # restrict midi values to predefined ranges
        if midi_value <= roli_settings.midi_value_range[0]:
            midi_value = roli_settings.midi_value_range[0] + 2
        if midi_value >= roli_settings.midi_value_range[1]:
            midi_value = roli_settings.midi_value_range[1] - 2
        # shift the midi_value to have the minimum value at zero in order to be used as a linspace index
        midi_value = midi_value - roli_settings.midi_value_range[0]
        linspace = slider.get_linspace()
        new_param_value = linspace[midi_value]
        self.synth.set_current_param(slider.get_name(), new_param_value, note)

    def __attach_callback__(self):
        self.inport.callback = self.__midi_roli_callback__

    def __remove_callback__(self):
        self.inport.callback = None


class RoliSettings:
    """
    This class holds the information for every roli dimension
    """
    def __init__(self, midi_value_range, centered_values, attached_slider=None):
        """
        Initialize the values asociated with a roli dimension

        Parameters
        ----------
        midi_value_range : [int]
            The extend of the input midi values at a specific dimension
        centered_values : bool
            A flag that indicates is the midi values are spread evenly around the initial point or if the initial point is a max or min midi value
        attached_slider: Slider
            The attached slider object that holds the information for synthesis
        """
        self.midi_value_range = midi_value_range
        self.centered_values = centered_values
        self.attached_slider = attached_slider
        self.enabled = False
