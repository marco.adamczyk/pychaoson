import pychaoson.sensitivity_calibration as sensitivity_calibration
import sc3nb as scn
import pychaoson.sliders as sliders
import pychaoson.config as config
import pychaoson.chaotic_systems as chaotic_systems
import pychaoson.sc_helper as sc_helper
import pychaoson.chaos_output_store as chaos_output_store
import pychaoson.exceptions as exceptions
import librosa
import numpy as np
from sc3nb import Buffer


class Synth:
    """
    This class controls the synthesis of the chaotic systems, its parameter control  and provides information for visualiazions
    """
    def __init__(self, sc, chaotic_system, playtype, sample_dict=None, sample_name='', buffer_dict=None):
        """
        Init the synth by setting the synthdefs and sliders.

        Parameters
        ----------
        sc : SC
            default SC instance
        chaotic_system : Chaotic_System
            An instance of a chaotic system
        playtype : str
            The selected synthesis type.
        sample_dict : dict, optional
            A holds the sample files for all samples and notes. Needed when playtype set to granular
        sample_name : str, optional
            The selected sample name. Needed when playtype set to granular. Needed when playtype set to granular
        buffer_dict : str, optional
            Stores the sc buffer object for every key of the selected sample_name. Needed when playtype set to granular
        """
        self.sc = sc
        self.sr = int(sc.server.nominal_sr)
        self.chaotic_system = chaotic_system
        self.playtype = playtype
        self.poi_list = []
        self.selected_poi = None
        self.playing = False
        self.is_precomputing = False
        self.sample_dict = sample_dict
        self.current_sample_name = sample_name
        self.buffer_dict = buffer_dict
        self.synth_dict = dict()
        self.synth_name_dict = dict()
        self.synth_def_dict = dict()
        self.bufnum_dict = dict()
        self.chaotic_sliders_dict = dict()
        self.custom_sliders_dict = dict()
        self.all_sliders_dict = dict()
        self.value_manager = chaos_output_store.ValueManager(sc, self)
        self.__init_synth__()

    def play_note(self, key):
        """
        Start playing a synth from the passed key. Always take the start values from the sliders as parameters.

        Parameters
        ----------
        key : int
            Selects the synth.
        """
        if len(self.synth_dict.keys()) == 0:
            for slider in list(self.chaotic_sliders_dict.values()):
                slider.set_current_value_to_start_value()
        if key not in self.synth_dict.keys():
            transformed_values = sensitivity_calibration.transform_start_values(list(self.chaotic_sliders_dict.values()), self.poi_list)
            control_dict = dict()
            if self.playtype == 'Granular':
                control_dict = {
                "bufnum": self.bufnum_dict[key],
                "gate": 1,
                "pan": 0,
                "useSeq": 0,
                "cycleBufnum": self.value_manager.cycle_buf.bufnum,
                "chaosParams": transformed_values}
            if self.playtype == 'Chaotic':
                control_dict = {
                "gate": 1,
                "chaosParams": transformed_values}
            control_dict = control_dict | sliders.get_custom_slide_control_dict(list(self.custom_sliders_dict.values()))
            self.synth_dict[key] = scn.Synth(name=self.synth_name_dict[key],
                                                controls=control_dict)
            self.playing = True

    def release_note(self, key):
        """
        Stop playing the synth at the passed key. If synth at the key value is not playing, nothing happens

        Parameters
        ----------
        key : int
            Selects the synth.
        """
        if key in self.synth_dict.keys():
            synth = self.synth_dict[key]
            synth.set("gate", 0)
            del self.synth_dict[key]
            if len(self.synth_dict.keys()) == 0:
                # set the playing flag to false
                self.playing = False

    def set_current_param(self, slider_name, value, key):
        """
        Seting the current value of a variable in the synth. Only works if the synth at the given key is playing currently.

        Parameters
        ----------
        slider_name : int
            The name of the slider the variable is bound to
        value : int
            The value of the variable
        key : int
            Defines the specific synth that should be targeted
        """
        slider = self.all_sliders_dict[slider_name]
        if key in self.synth_dict.keys():
            slider.set_current_value(key, value)
            if type(slider) == sliders.SlidableParameter:
                # if granular than use the cycle buffer to determine the next grain
                if self.playtype == 'Granular':
                    # if its still the start value then not use the cycle buffer to preserve start chaos
                    if slider.get_current_value(key) != slider.get_start_value():
                        # get the start and end index at which the phasor in the cycle buffer should repeat
                        chaos_param = self.get_chaos_current_param_of_note(key)
                        # already transformed in the precomputation no worries
                        start = self.value_manager.cycle_start_dict[str(chaos_param)]
                        end = self.value_manager.cycle_end_dict[str(chaos_param)]
                        # init the control dict
                        control_dict = dict()
                        control_dict['startBufFrame'] = start
                        control_dict['endBufFrame'] = end
                        control_dict['useSeq'] = 1
                        # ser the control dict
                        self.synth_dict[key].set(control_dict)
                else:   
                    # apply the transform on the new param configuration
                    chaos_param_transformed = sensitivity_calibration.transform_current_values(list(self.chaotic_sliders_dict.values()), self.poi_list, key)
                    control_dict = {"chaosParams": chaos_param_transformed}
                    self.synth_dict[key].set(control_dict)         
            if type(slider) == sliders.CustomSlidableParameter:
                # set the custom variable to a new value on the synth
                self.synth_dict[key].set(slider.synth_mapping_variable, value)
        else:
            raise Exception("Can only set current value when the synthesis on the key is running")
    
    def get_active_notes(self):
        """
        Gets the note that are currently playing

        Return
        ----------
        active_notes : [int]
            A list of midi nots as integer values
        """
        return list(self.synth_dict.keys())
    
    def set_sample(self, sample_name):
        """
        Set a new sample to be used by the synth. The name should already being processed by the pychaoson method. If you added a new one restart the api completely.
        All available samples can be found with pychaoson.get_sample_names()
        
        Parameters
        ----------
        sample_name : int
            The name of the new sample
        """
        if sample_name not in self.sample_dict.keys():
            raise exceptions.NoSuchSampleException('The sample "{}" is not available. Check your samples folder')
        samples = self.sample_dict[sample_name]
        self.current_sample_name = sample_name
        # reload buffers
        # free each buffer that exists
        for key in self.buffer_dict.keys():
            self.buffer_dict[key].free()
                    # clean dict
        self.bufnum_dict = dict()
        # add every sample pitch to a buffer
        for key in samples.keys():
            buf = Buffer().read(samples[key], channels=[0])
            self.buffer_dict[key] = buf
            # save the buffer number in a dictionary
            self.bufnum_dict[key] = buf.bufnum

    def set_chaos_start_params(self, values, slider_names):
        """
        Seting multiple start parameters of the chaotic space at once. This is more efficient because the precomutation step
        is only called once after the seting of the variable
        Parameters
        ----------
        values : [float]
            The values for the start parameters of the sliders that represent a dim in the parameter space of the chaotic system
        slider_names : [str]
            The names of the slidernames that values should be set
        """
        for i in range(len(slider_names)):
            self.chaotic_sliders_dict[slider_names[i]].set_start_value(values[i])
            self.chaotic_sliders_dict[slider_names[i]].compute_linspace()
        self.__precompute_chaos_output__()
        for i in range(len(slider_names)):
            self.chaotic_sliders_dict[slider_names[i]].set_current_value_to_start_value()

    def set_start_param(self, value, slider_name):
        """
        Set the one variable of the synth variables.

        Parameters
        ----------
        value: float
            The new value of the variable
        slider_name : str
            The name of the slider that represents the variable you want to set
        """
        self.all_sliders_dict[slider_name].set_start_value(value)
        self.all_sliders_dict[slider_name].compute_linspace()
        if type(self.all_sliders_dict[slider_name]) == sliders.SlidableParameter:
            self.__precompute_chaos_output__()
            self.all_sliders_dict[slider_name].set_current_value_to_start_value()

    def set_sensitivity_param(self, value, slider_name):
        """
        Set the sensitivity variable of a given slider. It control the spread of the generated linspace

        Parameters
        ----------
        value: float
            The new value of the sensitivity
        slider_name : str
            The name of the slider that you want to change
        """
        self.all_sliders_dict[slider_name].set_linear_sensitivity(value)
        self.all_sliders_dict[slider_name].compute_linspace()
        if type(self.all_sliders_dict[slider_name]) == sliders.SlidableParameter:
            self.__precompute_chaos_output__()
            self.all_sliders_dict[slider_name].set_current_value_to_start_value()

    def set_reverse_param(self, value, slider_name):
        """
        Set the reverse variable of a given slider. It control wether the linspace should go from low to high or
        from high to low.

        Parameters
        ----------
        value: float
            The new value of the sensitivity
        slider_name : str
            The name of the slider that you want to change
        """
        self.all_sliders_dict[slider_name].reverse = value
        self.all_sliders_dict[slider_name].compute_linspace()
        if type(self.all_sliders_dict[slider_name]) == sliders.SlidableParameter:
            self.__precompute_chaos_output__()
            self.all_sliders_dict[slider_name].set_current_value_to_start_value()

    def get_chaos_start_param(self, dims=None):
        """
        Gets the start values of the variables of the chaotic system.

        Parameters
        ----------
        dims: [int], optional
            Only returns the values of the given dimension
        """
        chaotic_sliders_list = list(self.chaotic_sliders_dict.values())
        if dims is None:
            return [chaos_slide.get_start_value() for chaos_slide in chaotic_sliders_list]
        else:
            start_param_list = []
            for dim in dims:
                start_param_list.append(chaotic_sliders_list[dim].get_start_value())
            return start_param_list
    
    def get_chaos_current_param_of_note(self, note, dims=None):
        """
        Gets the start values of the variables of the chaotic system.

        Parameters
        ----------
        dims: [int], optional
            Only returns the values of the given dimension
        """
        chaotic_sliders_list = list(self.chaotic_sliders_dict.values())
        if dims is None:
            return [chaos_slide.get_current_value(note) for chaos_slide in chaotic_sliders_list]
        else:
            start_param_list = []
            for dim in dims:
                start_param_list.append(chaotic_sliders_list[dim].get_current_value(note))
            return start_param_list
        
    def get_chaos_start_transformed_param(self, dims=None):
        """
        Gets the start values of the variables of the chaotic system but already transformed by the sensitivity calibration.

        Parameters
        ----------
        dims: [int], optional
            Only returns the values of the given dimension

        Return
        ----------
        transformed_start_values : [float]
            The transformed start values of the chaotic system
        """
        chaotic_sliders_list = list(self.chaotic_sliders_dict.values())
        transformed_start_values = sensitivity_calibration.transform_start_values(chaotic_sliders_list, self.poi_list)
        dim_transformed_start_values = []
        if dims is not None:
            for dim in dims:
                dim_transformed_start_values.append(transformed_start_values[dim])
            return dim_transformed_start_values
        return transformed_start_values

    def get_chaos_current_transformed_parameters(self, dims=None):
        """
        Gets the current values transformed by the sensitivity calibration of all variables of the chaotic system at every note that is activly played.

        Parameters
        ----------
        dims: [int], optional
            Only returns the values of the given dimension
        
        Return
        ----------
        current_points : [float]
            The current values of the synths that are currently played
        """
        notes_on = self.synth_dict.keys()
        current_points = []
        chaotic_sliders_list = list(self.chaotic_sliders_dict.values())
        for note in notes_on:
            current_points.append(sensitivity_calibration.transform_current_values(chaotic_sliders_list, self.poi_list, note))
        if dims is not None:
            dim_current_points = []
            for point in current_points:
                dim_point = []
                for dim in dims:
                    dim_point.append(point[dim])
                dim_current_points.append(dim_point)
            return dim_current_points
        return current_points


    def get_chaos_current_param(self, dims=None):
        """
        Gets the current values of all variables of the chaotic system at every note note. No transformation is done

        Parameters
        ----------
        dims: [int], optional
            Only returns the values of the given dimension

        Return
        ----------
        current_points : [float]
            The current values of the synths that are currently played
        """
        chaotic_sliders_list = list(self.chaotic_sliders_dict.values())
        notes_on = self.synth_dict.keys()
        current_points = []

        if dims is None:
            for note in notes_on:
                current_points.append([chaos_slide.get_current_value(note) for chaos_slide in chaotic_sliders_list])
        else:
            for note in notes_on:
                current_param_list = []
                for dim in dims:
                    current_param_list.append(chaotic_sliders_list[dim].get_current_value(note))
                current_points.append(current_param_list)
        return current_points
    
    def get_slider_names(self):
        """
        Gets the value range of the variable attached to a slider object

        Return
        ----------
        slider_names : str
            The name of the sliders
        """
        return list(self.all_sliders_dict.keys())
    
    def get_slider_start_value(self, slider_name):
        """
        Get the start value of the slider

        Parameters
        ----------
        slider_name: str
            The name of the slider

        Return
        ----------
        start_value : float
            Represents the value when the synth starts playing
        """
        return self.all_sliders_dict[slider_name].get_start_value()

    def get_slider_value_range(self, slider_name):
        """
        Gets the value range of the variable attached to a slider object

        Parameters
        ----------
        slider_name: str
            The name of the slider

        Return
        ----------
        value_range : (float, float)
            The value range of the variable. From low to high
        """
        slider = self.all_sliders_dict[slider_name]
        return [slider.get_min_value(), slider.get_max_value()]
    
    def get_slider_reverse(self, slider_name):
        """
        Gets the information if the created linspace should go from low to high or from high to low

        Parameters
        ----------
        slider_name: str
            The name of the slider

        Return
        ----------
        reverse : bool
            A boolean that represents how the linspace is structured.
        """
        return self.all_sliders_dict[slider_name].get_reverse()

    def get_slider_sensitivity(self, slider_name):
        """
        Gets the information how much space of the value range the linspace should cover. Can range from 0 - 1

        Parameters
        ----------
        slider_name: str
            The name of the slider

        Return
        ----------
        sensitivity : float
            Represents the space covered by the created linspace in the slider
        """
        return self.all_sliders_dict[slider_name].get_sensitivity()
    
    def get_chaotic_system_name(self):
        """
        Gets the name of the chaotic system that is used by this synth.

        Parameters
        ----------
        slider_name: str
            The name of the slider

        Return
        ----------
        chaotic_system_name : float
            The name of the used chaotic system used in this synth object
        """
        return self.chaotic_system.get_name()

    def get_currently_played_notes(self):
        """
        Gets the notes midi values that are currently played 

        Return
        ----------
        played_notes : [int]
            The currently played midi notes
        """
        return self.synth_dict.keys()

    def add_poi(self, point, radius=.1, exp=2, normalize=True, repelling=False):
        """
        Adds point of interest that effects the chaotic variable space of the synth

        Parameters
        ----------
        point: [float]
            The values represent the point in the parameter space
        radius: flaot
            The space the point should cover in parameter space
        normalize: bool, optional
            If set to true the values are getting normalized. If false it is assumed thez are alreadz normalized
        repelling: bool, optional
            Then set to true the effect reverses the space is that region is less dense
        """
        if normalize:
            point = sensitivity_calibration.normalize_custom_point(point, list(self.chaotic_sliders_dict.values()))
        if any([np.array_equal(poi.poi, point) for poi in self.poi_list]):
            raise Exception("Point is already a point of interest")
        self.poi_list.append(sensitivity_calibration.PointOfInterest(point, radius, exp, repelling))

    def remove_poi(self, poi):
        """
        Removes a point of interest that effects the chaotic variable space of the synth

        Parameters
        ----------
        poi: PointOfInterest
            A point of interest instance that is set in this synth.
        """
        index_to_remove = None
        for i in range(len(self.poi_list)):
            if self.poi_list[i] == poi:
                index_to_remove = i
        if index_to_remove is not None:
            #if this poi is the selected point, then remove the selected point
            if self.poi_list[index_to_remove] == self.selected_poi:
                self.selected_poi = None
            del self.poi_list[index_to_remove]

    def get_pois(self):
        """
        Get all the points of interests set in this synth instance. 

        Return
        ----------
        poi_list : [PointOfInterest]
            A list of PointOfInterest instances
        """
        return self.poi_list

    def get_selected_poi(self):
        """
        Get the selected point of interest instance. Can be used for visualization. None if no point of interest is selected

        Return
        ----------
        poi : PointOfInterest
            The selected PointOfInterest instances
        """
        return self.selected_poi
    
    def set_selected_poi(self, poi):
        """
        Get all the points of interests set in this synth instance. 

        Parameters
        ----------
        poi: PointOfInterest
            A point of interest instance that is already part of this synth
        """
        self.selected_poi = poi 

    def __create_chaotic_synths__(self):   
            # create synth for every key
            self.synth_name_dict = dict()
            for key in range(config.LOWEST_MIDI_NOTE, config.HIGHEST_MIDI_NOTE + 1, 1):
                chaotic_synth_def = sc_helper.get_chaotic_synth_def()
                # remove specifiers from synth def
                chaotic_synth_def.set_context("VALUEGENERATOR",
                                                    self.chaotic_system.get_sc_generator())
                chaotic_synth_def.set_context("CHAOS_PARAMS",
                                                    str(self.chaotic_system.get_params()))
                chaotic_synth_def.set_context("INIT_VALUES",
                                                    chaotic_systems.get_init_values_as_args(self.chaotic_system))
                # add a frequency for every key 
                chaotic_synth_def.set_context("FREQENCY", 
                                                str(librosa.midi_to_hz(key)))
                #add synth to synth def
                chaotic_synth_name = "chaotic_synth_" + str(key)
                chaotic_synth_def.add(name=chaotic_synth_name)
                self.synth_name_dict[key] = chaotic_synth_name
                self.synth_def_dict[key] = chaotic_synth_def

    def __create_granular_synths__(self):
        # create synth for every key
        self.synth_name_dict = dict()
        for key in self.sample_dict[self.current_sample_name].keys():
            granular_synth_def = sc_helper.get_granular_synth_def()
            # add the specifiers
            granular_synth_def.set_context("BUFFNUM", str(self.buffer_dict[key].bufnum))
            granular_synth_def.set_context("GRAINSELECTOR",
                                                self.chaotic_system.get_sc_generator_for_grains())
            granular_synth_def.set_context("CHAOS_PARAMS",
                                                str(self.chaotic_system.get_params()))
            granular_synth_def.set_context("INIT_VALUES",
                                                chaotic_systems.get_init_values_as_args(self.chaotic_system))
            granular_synth_name = "granular_" + str(key)
            # add the synth to server
            granular_synth_def.add(name=granular_synth_name)
            # add to dict with the midi key as the key and the name of the synthesizer in sc as value
            self.synth_name_dict[key] = granular_synth_name
            self.synth_def_dict[key] = granular_synth_def

    def __init_synth__(self):
        self.sc.server.free_all()
        if self.playtype == 'Granular':
            self.set_sample(self.current_sample_name)
            self.__create_granular_synths__()
            amp_slider = sliders.get_amp_slider()
            # Granular synthesis is often quiet
            amp_slider.set_start_value(1.2)
            custom_sliders = [sliders.get_custom_grain_len_slider(), 
                              sliders.get_custom_overlap_slider(),
                              sliders.get_custom_reverb_mix_slider(),
                              amp_slider]
        elif self.playtype == 'Chaotic':
            self.__create_chaotic_synths__()
            amp_slider = sliders.get_amp_slider()
            # chaotic synthesis is very loud
            amp_slider.set_start_value(0.3)
            custom_sliders = [sliders.get_custom_reverb_mix_slider(),
                              amp_slider]
        else:
            raise exceptions.NoSuchPlaytypeException('The playtype "{}" does not exist'.format(self.playtype))
        chaotic_sliders = sliders.map_chaos_parameters_to_slides(self.chaotic_system)
        for chaotic_slider in chaotic_sliders:
            self.chaotic_sliders_dict[chaotic_slider.get_name()] = chaotic_slider
        for custom_slider in custom_sliders:
            self.custom_sliders_dict[custom_slider.get_name()] = custom_slider
        self.chaotic_sliders_dict
        self.all_sliders_dict = self.custom_sliders_dict | self.chaotic_sliders_dict
        self.__precompute_chaos_output__()

    def __precompute_chaos_output__(self):
        # precompute values the user can reach with roli sliders
        self.is_precomputing = True
        self.value_manager.update_accessible_values(sliders.get_all_accessible_params(self.chaotic_sliders_dict))
        self.is_precomputing = False
