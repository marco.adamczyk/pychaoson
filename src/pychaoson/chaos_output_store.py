from sc3nb import Buffer
import pychaoson.utilities as utilities
import pychaoson.sensitivity_calibration as sensitivity_calibration
import pychaoson.sc_helper as sc_helper
import numpy as np

class ValueManager:
    """
    This class holds the information about the output of the chaotic systems and computes found cycles in the output. Also does
    a computation of the output and found cycles in non realtime for the parameter combinations that are expected to occur in realtime. 

    this precomputations is needed when the output should be displayed in a plot or the granular synthesis should go directl into the chaotic system output loop
    when the current parameter of the system changes. 
    """
    def __init__(self, sc, synth):
        """
        Initializes all necessary dictionaries and variables that are needed to store the output values and the resulting calculated information.

        Parameters
        ----------
        sc : SC
            A SC instance
        synth : Synth
            A Synth instance
        """
        self.sc = sc
        self.synth = synth
        self.cycle_buf_values = None
        self.cycle_buf = None
        # gets the index pointing to the buffer string where the sequence starts
        self.cycle_start_dict = dict()
        # gets the index pointing to the buffer string where the sequence ends
        self.cycle_end_dict = dict()
        self.cycle_dict = dict()
        self.init_dict = dict()
        self.last_time_series_dict = dict()

    def update_accessible_values(self, linspaced_params_prod, take_last=1000, take_first=100, append_last=50):
            """
            Precomputes the chaotic system output for a list of parameter combinations and tries to find a cycle in the output values. Saving those
            results into the class variables.

            Parameters
            ----------
            linspaced_params_prod : [[float]]
                The combination of values in the parameter space of the chaotic system
            take_last : int, optional
                The number of last values to take from the whole computed output of the chaotic system
            take_first : int, optional
                A Synth instance
                The number of first values to take from the whole computed output of the chaotic system
            append_last : int, optional
                The number of first values to take from the whole and save to the dict.
            """
            # clean the dictionaries
            self.cycle_start_dict = dict()
            self.cycle_end_dict = dict()
            self.cycle_dict = dict()
            self.init_dict = dict()
            self.last_time_series_dict = dict()
            linspaced_params = [sensitivity_calibration.transform_custom_values(np.atleast_1d(param), 
                                                                                list(self.synth.chaotic_sliders_dict.values()), 
                                                                                self.synth.poi_list) 
                                for param in linspaced_params_prod]
            # compute time series for the parameter combinations
            first_time_series_list, last_time_series_list = sc_helper.record_chaotic_system(
                                    self.sc,
                                    self.synth.sr,
                                    self.synth.chaotic_system, 
                                    linspaced_params, 
                                    timing_delta=0.05, 
                                    take_last=take_last,
                                    take_first=take_first,
                                    prefix_file_name='linspace',
                                    enable_tqdm=False,
                                    sample_format='float64')
            cycle_buf_values = []
            index = 0

            for i in range(len(last_time_series_list)): 
                linspaced_params_prod_i = np.atleast_1d(linspaced_params_prod[i])
                time_series = last_time_series_list[i]
                cycle = utilities.find_cycle(time_series)
                self.cycle_start_dict[str(list(linspaced_params_prod_i))] = index
                if cycle is None:
                    # no cycle found
                    cycle_buf_values += list(time_series)
                    index += len(time_series)
                    self.init_dict[str(list(linspaced_params_prod_i))] = first_time_series_list[i]
                else:
                    # cycle found
                    cycle_buf_values += cycle
                    index += len(cycle)
                    cycle_init_value = cycle[0]
                    # try to find the cycle start in the init values
                    try:
                        cycle_start_index = np.min(np.nonzero(first_time_series_list[i] == cycle_init_value)[0])
                        # if found only take the element until the cycle sets in
                        self.init_dict[str(list(linspaced_params_prod_i))] = first_time_series_list[i][:cycle_start_index + 1]
                    except ValueError as e:
                        # if not found take the whole init array
                        self.init_dict[str(list(linspaced_params_prod_i))] = first_time_series_list[i]
                
                self.cycle_end_dict[str(list(linspaced_params_prod_i))] = index - 1
                self.cycle_dict[str(list(linspaced_params_prod_i))] = cycle
                self.last_time_series_dict[str(list(linspaced_params_prod_i))] = time_series[len(last_time_series_list[i]) - append_last:]
            if self.cycle_buf is not None:
                self.cycle_buf.free()
            self.cycle_buf_values = cycle_buf_values
            self.cycle_buf = Buffer().load_data(np.array(cycle_buf_values, dtype=np.float64))

