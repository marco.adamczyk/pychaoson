from abc import ABC, abstractmethod


def load_kr_systems():
    """
    Initialzes all chaotic systems.

    Returns
    -------
    chaotic_systems_dict: dict
        A dict with the keys being the chaotic system name and the values the chaotic system instances
    """
    chaotic_systems_dict = dict()

    logistic_map = LogisticMap()
    logistic_map_name = logistic_map.get_name()
    hennon_map = HennonMap()
    hennon_map_name = hennon_map.get_name()
    lincong_map = LinCongN()
    lincong_map_name = lincong_map.get_name()
    quad_map = QuadN()
    quad_map_name = quad_map.get_name()
    lorenz_map = LorenzL()
    lorenz_map_name = lorenz_map.get_name()
    fbsine_map = FbSine()
    fbsine_map_name = fbsine_map.get_name()
    standard_map = Standard()
    standard_map_name = standard_map.get_name()

    chaotic_systems_dict[logistic_map_name] = logistic_map
    chaotic_systems_dict[hennon_map_name] = hennon_map
    chaotic_systems_dict[lincong_map_name] = lincong_map
    chaotic_systems_dict[quad_map_name] = quad_map
    chaotic_systems_dict[lorenz_map_name] = lorenz_map
    chaotic_systems_dict[fbsine_map_name] = fbsine_map
    chaotic_systems_dict[standard_map_name] = standard_map
    return chaotic_systems_dict

def get_init_values_as_args(chaotic_system):
    """
    Transform the intial values of the chaotic system into the form of a synthdef definition.
    
    Parameters
    ----------
    chaotic_system : ChaoticSystem
        A chaotic system instance

    Returns
    -------
    arg_str: str
        The argument string for the synthdef
    """
    args_str = ''
    init_value_names = chaotic_system.get_init_value_names()
    init_values = chaotic_system.get_init_values()
    for i in range(len(init_value_names)):
        args_str += init_value_names[i] + '=' + str(init_values[i]) + ', '
    return args_str


class ChaoticMap(ABC):
    """
    An abstract class for the chaotic system that defines which methods should be implemented. Every chaotic map represents an chaotic sc3 generator.
    """

    @abstractmethod
    def get_params(self):
        """
        Get the parameters of the system

        Returns
        -------
        params: [float]
            A list of the values of the system
        """
        pass

    @abstractmethod
    def get_param_space(self):
        """
        Gets for every parameter dimension the value, the min value, the max value and the dim name
        
        Returns
        -------
        param_space: [(float, float, float, str)]
            A list of the values of the system
        """
        pass

    @abstractmethod
    def get_init_value_names(self):
        """
        Gets the names of the init parameters in the sc3 definition
        
        Returns
        -------
        init_value_names: [str]
            The names of the init value variables
        """
        pass

    @abstractmethod
    def get_init_values(self):
        """
        Gets the inital values of the init parameters in the sc3 definition
        
        Returns
        -------
        init_values: [int]
            The values of the init value variables
        """
        pass

    @abstractmethod
    def get_name(self):
        """
        Get the name of the chaotic synthesizer.
        
        Returns
        -------
        init_values: [int]
            The values of the init value variables
        """
        pass

    @abstractmethod
    def get_plot_dim_names(self):
        """
        Gets the parameter names of the parameter of the chaotic system that should be ploted by the visualization
        
        Returns
        -------
        plot_dim_names: [str]
            The names of the parameter to plot
        """
        pass

    @abstractmethod
    def get_sc_generator(self, init_values=None):
        """
        Get the ugen definition for the sc3 generator when the synthesis type is set to chaotic audio rate generation
        
        Parameters
        ----------
        init_values : [float]
            The init values of the system

        Returns
        -------
        sc_generator_str: str
            A string that represents the generator in sc3 code
        """
        pass

    @abstractmethod
    def get_sc_generator_for_grains(self, init_values=None):
        """
        Get the ugen definition for the sc3 generator when the synthesis type is set to granular synthesis
        

        Parameters
        ----------
        init_values : [float]
            The init values of the system
            
        Returns
        -------
        sc_generator_str: str
            A string that represents the generator in sc3 code
        """
        pass


class LogisticMap(ChaoticMap):
    def __init__(self, init_param=None, init_value=0.5):
        if init_param is None:
            init_param = 3.5
        self.param = init_param
        self.init_value = init_value
        self.min_value = 2.9
        self.max_value = 4

    def get_params(self):
        return [self.param]

    def get_param_space(self):
        return [(self.param, self.min_value, self.max_value, 'Dim0')]

    def get_init_value_names(self):
        return ['init']

    def get_name(self):
        return "Logistic Map"
    
    def get_init_values(self):
        return [0.5]

    def get_plot_dim_names(self):
        return ['Dim0']
    
    def get_plot_dim(self):
        return [0]
    
    def get_derivative(self):
        return lambda x, r: r - 2 * r * x
        
    def get_sc_generator(self, init_values=None):
        if init_values is None:
            init_value = self.init_value
        else:
            init_value = init_values[0]
        return "Logistic.ar(chaosParams.at(0), freq: freq, init: " + str(init_value) + ", mul: 2, add: -1)"

    def get_sc_generator_for_grains(self, init_values=None):
        if init_values is None:
            init_value = self.init_value
        else:
            init_value = init_values[0]
        return "Logistic.ar(chaosParams.at(0), freq: freq, init: " + str(init_value) + ")"



class HennonMap(ChaoticMap):
    def __init__(self, init_param=None, init_value=None):
        if init_value is None:
            init_value = [0, 1]
        if init_param is None:
            init_param = [1.4, -0.1]

        self.init_param = init_param
        self.param = init_param
        self.init_value = init_value

    def get_params(self):
        return self.param

    def get_param_space(self):
        return [(self.init_param[0], 0.7, 2.2, 'Dim0'),
                 (self.init_param[1], -0.4, 0.6, 'Dim1')]

    def get_init_value_names(self):
        return ['x0', 'x1']
    
    def get_init_values(self):
        return self.init_value

    def get_name(self):
        return "Hennon Map"

    def get_plot_dim_names(self):
        return ['Dim0', 'Dim1']
    
    def get_plot_dim(self):
        return [0, 1]

    def get_sc_generator_for_grains(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
        else:
            init_values = init_values
        return ("HenonN.ar(freq: freq, a: chaosParams.at(0), b: chaosParams.at(1)" +
                ", x0: x0" +
                ", x1: x1" +
                ", mul: 0.5" +
                ", add: 0.5)")
    
    def get_sc_generator(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
        else:
            init_values = init_values
        return ("HenonN.ar(freq: freq, a: chaosParams.at(0), b: chaosParams.at(1)" +
                ", x0: x0" +
                ", x1: x1" +
                ", mul: 1" +
                ", add: 0)")

        


class LinCongN(ChaoticMap):
    def __init__(self, init_param=None, init_value=None):
        if init_value is None:
            init_value = [0]
        if init_param is None:
            init_param = [ 1.1, 0.13, 1]

        self.init_param = init_param
        self.param = init_param
        self.init_value = init_value

    def get_params(self):
        return self.param

    def get_param_space(self):
        return [(self.init_param[0], 0.3, 1.5, 'Dim0'), 
                (self.init_param[1], -1, 1, 'Dim1'), 
                (self.init_param[2], 1, 2, 'Dim2')]

    def get_plot_dim_names(self):
        return ['Dim0', 'Dim2']
    
    def get_plot_dim(self):
        return [0, 2]
    
    def get_init_value_names(self):
        return ['xi']
    
    def get_init_values(self):
        return self.init_value

    def get_name(self):
        return "LinCong Map"

    def get_sc_generator(self, init_values=None):
        if init_values is None:
            init_values = self.init_value

            init_values = init_values
        return ("LinCongN.ar(freq: freq, a: chaosParams.at(0), b: chaosParams.at(1), m: chaosParams.at(2)" +
                ", xi: " + str(init_values[0]) +
                ", mul: 1" +
                ", add: 0)")    
    
    def get_sc_generator_for_grains(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
            init_values = init_values
        return ("LinCongN.ar(freq: freq, a: chaosParams.at(0), b: chaosParams.at(1), m: chaosParams.at(2)" +
                ", xi: " + str(init_values[0]) +
                ", mul: 0.5" +
                ", add: 0.5)")


class QuadN(ChaoticMap):
    def __init__(self, init_param=None, init_value=None):
        if init_value is None:
            init_value = [0]
        if init_param is None:
            init_param = [ 1, -1, -0.75]

        self.init_param = init_param
        self.param = init_param
        self.init_value = init_value

    def get_params(self):
        return self.param

    def get_param_space(self):
        return [(self.init_param[0], 0, 2, 'Dim0'), 
                (self.init_param[1], -2, 0, 'Dim1'), 
                (self.init_param[2], -1, 0, 'Dim2')]

    def get_name(self):
        return "Quad Map"
    
    def get_plot_dim_names(self):
        return [ 'Dim0', 'Dim1']
    
    def get_plot_dim(self):
        return [0, 1]

    def get_init_value_names(self):
        return ['xi']
    
    def get_init_values(self):
        return self.init_value
    
    def get_sc_generator_for_grains(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
            init_values = init_values
        return ("QuadN.ar(freq: freq, a: chaosParams.at(0), b: chaosParams.at(1), c: chaosParams.at(2)" +
                ", xi: " + str(init_values[0]) +
                ", mul: 0.5" +
                ", add: 0.5)")
    
    def get_sc_generator(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
            init_values = init_values
        return ("QuadN.ar(freq: freq, a: chaosParams.at(0), b: chaosParams.at(1), c: chaosParams.at(2)" +
                ", xi: " + str(init_values[0]) +
                ", mul: 1" +
                ", add: 0)")
    

class LorenzL(ChaoticMap):
    def __init__(self, init_param=None, init_value=None):
        if init_value is None:
            init_value = [ 0.1, 0.0, 0.0]
        if init_param is None:
            init_param = [8, 28, 2.667]

        self.init_param = init_param
        self.param = init_param
        self.init_value = init_value

    def get_params(self):
        return self.param

    def get_param_space(self):
        return [(self.init_param[0], 4, 21, 'Dim0'), 
                (self.init_param[1], 20, 38, 'Dim1'), 
                (self.init_param[2], 0, 3, 'Dim2')]

    def get_name(self):
        return "Lorenz Map"
    
    def get_plot_dim_names(self):
        return [ 'Dim0', 'Dim2']
    
    def get_plot_dim(self):
        return [0, 2]

    def get_init_value_names(self):
        return ['xi', 'yi', 'zi']
    
    def get_init_values(self):
        return self.init_value
    
    def get_sc_generator_for_grains(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
            init_values = init_values
        return ("LorenzL.ar(freq: freq, s: chaosParams.at(0), r: chaosParams.at(1), b: chaosParams.at(2), h: 0.08" +
                ", xi: " + str(init_values[0]) +
                ", yi: " + str(init_values[1]) +
                ", zi: " + str(init_values[2]) +
                ", mul: 0.5" +
                ", add: 0.5)")
    
    
    def get_sc_generator(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
            init_values = init_values
        return ("LorenzL.ar(freq: freq, s: chaosParams.at(0), r: chaosParams.at(1), b: chaosParams.at(2), h: 0.08" +
                ", xi: " + str(init_values[0]) +
                ", yi: " + str(init_values[1]) +
                ", zi: " + str(init_values[2]) +
                ", mul: 1" +
                ", add: 0)")
    
class FbSine(ChaoticMap):
    def __init__(self, init_param=None, init_value=None):
        if init_value is None:
            init_value = [ 0.1, 0.1]
        if init_param is None:
            init_param = [1, 0.1, 1.1]

        self.init_param = init_param
        self.param = init_param
        self.init_value = init_value

    def get_params(self):
        return self.param

    def get_param_space(self):
        return [(self.init_param[0], 0, 10, 'Dim0'), 
                (self.init_param[1], 0, 1, 'Dim1'), 
                (self.init_param[2], 0, 3, 'Dim2')]

    def get_name(self):
        return "Fb Sine Map"
    
    def get_plot_dim_names(self):
        return [ 'Dim0', 'Dim1']
    
    def get_plot_dim(self):
        return [0, 1]

    def get_init_value_names(self):
        return ['xi', 'yi']
    
    def get_init_values(self):
        return self.init_value
    
    def get_sc_generator_for_grains(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
            init_values = init_values
        return ("FBSineN.ar(freq: freq, im: chaosParams.at(0), fb: chaosParams.at(1), a:chaosParams.at(2), c: 0.5" +
                ", xi: " + str(init_values[0]) +
                ", yi: " + str(init_values[1]) +
                ", mul: 0.5" +
                ", add: 0.5)")
    
    
    def get_sc_generator(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
            init_values = init_values
        return ("FBSineN.ar(freq: freq, im: chaosParams.at(0), fb: chaosParams.at(1), a:chaosParams.at(2), c: 0.5" +
                ", xi: " + str(init_values[0]) +
                ", yi: " + str(init_values[1]) +
                ", mul: 1" +
                ", add: 0)")
    
class Standard(ChaoticMap):
    def __init__(self, init_param=None, init_value=None):
        if init_value is None:
            init_value = [ 0.5, 0.0]
        if init_param is None:
            init_param = [1]

        self.init_param = init_param
        self.param = init_param
        self.init_value = init_value

    def get_params(self):
        return self.param

    def get_param_space(self):
        return [(self.init_param[0], 0.8, 4, 'Dim0')]

    def get_name(self):
        return "Standard Map"
    
    def get_plot_dim_names(self):
        return [ 'Dim0']
    
    def get_plot_dim(self):
        return [0]

    def get_init_value_names(self):
        return ['xi', 'yi']
    
    def get_init_values(self):
        return self.init_value
    
    def get_sc_generator_for_grains(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
            init_values = init_values
        return ("StandardN.ar(freq: freq, k: chaosParams.at(0)" +
                ", xi: " + str(init_values[0]) +
                ", yi: " + str(init_values[1]) +
                ", mul: 0.5" +
                ", add: 0.5)")
    
    
    def get_sc_generator(self, init_values=None):
        if init_values is None:
            init_values = self.init_value
            init_values = init_values
        return ("StandardN.ar(freq: freq, k: chaosParams.at(0)" +
                ", xi: " + str(init_values[0]) +
                ", yi: " + str(init_values[1]) +
                ", mul: 1" +
                ", add: 0)")
    