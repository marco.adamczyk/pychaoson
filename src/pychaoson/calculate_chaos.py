import itertools
import math
import numpy as np
from tqdm import tqdm
import os
from datetime import datetime
import pychaoson.config as config
import pychaoson.sc_helper as sc_helper
import pychaoson.sensitivity_calibration as sensitivity_calibration
import pychaoson.utilities as utilities
import json
import random


def dimension_analysis(pychaos, chaotic_system, dims, points_per_dim=40,
                                 take_num=1000,take_last=True, method='box_count_dim', custom_transform=None, no_transform=False, save=True):
    """
    Computes the box count dimension for a chaotic system parameter cominations.

    Parameters
    ----------
    pychaos : Pychaoson
        A pychaoson instance 
    chaotic_system_name : str
        The name of the chaotic sytem to be used
    dims : str, optional
        The dimensions that should be varied and then combination of the created.
    points_per_dim : str, optional
        The amount of point per dimension that should be used in the computation
    take_last : int, optional
        The number of output values that should be considered in this computation. From last on.
    method : str, optional
        The dimension analysis method
    custom_transform : func, optional
        Application of a custom transform of the parameter space
    no_transform : bool, optional
        When the flag is set to true, the transform will not be applyed
    save: bool, optional
        True, when the results are save in the file and loaded the next time

    
    Returns
    -------
    dimensions_array: np.array
        The 2D array that stores the dimension result for every parameter combination that should be considered.
    """
    long_id = ",".join([chaotic_system.get_name(), 
                str(dims), 
                str(points_per_dim), 
                str(take_last), 
                str(take_num),
                method,
                str(sensitivity_calibration.get_points_of_interest_dict(pychaos.synth.poi_list))])
    existing_data = __check_for_existing__data__(dims, points_per_dim, take_num, long_id)
    if existing_data is not None:
        return existing_data
    directory = os.path.join(config.DATA_DIR, "box_count_data")
    data_mapping_file = os.path.join(directory, 'mapping_file.json')
    def generate_unique_id(existing_ids):
        while True:
            new_id = random.randint(1, 100000)  # Adjust the range as needed
            if str(new_id) not in existing_ids:
                return str(new_id)
    
    existing_file_names =  utilities.get_file_names_from_dir(directory)
    name = generate_unique_id(existing_file_names)
    dimension_filename = name + ".npy"
    dimension_file = os.path.join(directory, dimension_filename)
    time_series_filename = name + ".mymemmap"
    time_series_file = os.path.join(directory, time_series_filename)
    if os.path.exists(dimension_file):
        os.remove(dimension_file)
    if os.path.exists(time_series_file):
        os.remove(time_series_file)

    # get the parameter matrix and the related indexes
    linspaced_params_prod, linspaced_params_idx = __get_product_from_params__(chaotic_system.get_param_space(), dims,
                                                                            points_per_dim)
    
    # if the parameter space is 1D assure that it is still a 1d array so that computation can be generalized
    param_combinations = [np.atleast_1d(linspaced_param_prod) for linspaced_param_prod in linspaced_params_prod]

    # apply custom transform to the parameter combinations. Used for testing new mapping techniques
    if custom_transform is not None:
        param_combinations = custom_transform(param_combinations)
    # get the a time series for every parameter combination
    time_series_array  = __get_time_series__(pychaos, chaotic_system, param_combinations, dims,
                                        time_series_file, take_num, take_last, no_transform)
    
    # initialize an empty array that stores the dimension information for every parameter combination
    dimensions_array = np.zeros(tuple(np.full((len(dims)), points_per_dim).tolist()))

    if method == "box_count_dim":
        dimensions_array = box_count_dim(linspaced_params_idx, time_series_array, dimensions_array)
    if method == "sample_entropy":
        dimensions_array = sample_entropy(linspaced_params_idx, time_series_array, dimensions_array)
    if method == "lyapunov_exponent":
        dimensions_array = lyapunov_exponent(linspaced_params_idx, param_combinations,time_series_array, dimensions_array, chaotic_system)
    
    # save the date if the flag is set
    if save:
        # save box count dimenssion to file
        np.save(dimension_file, dimensions_array)
        # Check if the file already exists
        try:
            with open(data_mapping_file, "r") as file:
                # If the file exists, read its contents into a dictionary
                data = json.load(file)
        except FileNotFoundError:
            # If the file doesn't exist, initialize an empty dictionary
            data = {}

        data[long_id] = name
        with open(data_mapping_file, "w") as file:
            json.dump(data, file, indent=4)

    return dimensions_array, time_series_array


def box_count_dim(linspaced_params_idx, time_series_array, dimensions_array):
# compute the box count dimension for every parameter combination
    for i in tqdm(range(len(linspaced_params_idx)), desc="Compute box dim"):
        
        param_idx = linspaced_params_idx[i]
        # get the indices of parameter for the dimension array

        # get the time series for the specific parametwer combination
        time_series = time_series_array[i, :]

        log_box_count_list = []
        log_box_size_list = []
        bin_count = 1
        # set the output dimension of the time series to 1 but the procedure is generalizable for n output dimensions
        output_dim = 1
        # set the bins in which the values of the time series could fall
        bins = np.full(output_dim, bin_count)
        multiplication = 2
        iterations = 10
        for j in range(iterations):
            # after every iteration increase the bin count by the factor of ten
            bin_count = bin_count * multiplication
            bins[:] = bin_count
            # compute a histogram with the curent bins
            h, _ = np.histogramdd(time_series, bins=tuple(bins))
            # count how many vin have actually values init
            non_zero_boxes = np.count_nonzero(h)
            # create list with the loged results
            log_box_count_list.append(np.log(non_zero_boxes))
            log_box_size_list.append(np.log(1 / bin_count))
        # fit the results to a line to get thebox count dimension
        m = np.polyfit(np.array(log_box_size_list), np.array(log_box_count_list), 1)[0]
        dimensions_array[tuple(param_idx)] = m

    return dimensions_array


def sample_entropy(linspaced_params_idx, time_series_array, dimensions_array):
    def sample_entropy_one_data(timeseries_data, window_size, r):
        # generate templates
        def get_templates(timeseries_data, m):
            return [timeseries_data[x:x + m] for x in range(0, len(timeseries_data) - m + 1)]

        # get the matches of tamples
        def get_matches(templates, r):
            count = 0
            for (template_1, template_2) in itertools.combinations(templates, 2):
                if matching(np.array(template_1), np.array(template_2), r):
                    count += 1
            return count

        # ceck if the tempaltes are matching
        def matching(template_1, template_2, r):
            return np.all(np.abs(template_1 - template_2) < r)

        B = get_matches(get_templates(timeseries_data, window_size), r)
        A = get_matches(get_templates(timeseries_data, window_size + 1), r)
        if B == 0 or A == 0:
            return 0
        return -math.log(A / B)
    
    for i in tqdm(range(len(linspaced_params_idx)), desc="Compute sample entropy"):
        param_idx = linspaced_params_idx[i]

        time_series = np.array(time_series_array[i, :])
        std = np.std(time_series)

        dimensions_array[tuple(param_idx)] = sample_entropy_one_data(time_series, 2, 0.2 * std)
    return dimensions_array

def lyapunov_exponent(linspaced_params_idx, param_combinations, time_series_array, dimensions_array, chaotic_system):
    derivative = chaotic_system.get_derivative()
    for i in tqdm(range(len(linspaced_params_idx)), desc="Compute lyapunov"):
        param_idx = linspaced_params_idx[i]

        time_series = np.array(time_series_array[i, :])
        lyapunov = 0
        if param_combinations[i] != 0:
            for j in range(len(time_series)):
                lyapunov += math.log(abs(derivative(time_series[j], param_combinations[i])))

        dimensions_array[tuple(param_idx)] = lyapunov
    return dimensions_array

def __get_product_from_params__(param_space, dims, points_per_dim):
    linspaced_params = []

    # create linspaces for every parameter dimension with specific length from min to max
    for dim_index in dims:
        linspaced_param = np.linspace(param_space[dim_index][1], param_space[dim_index][2], points_per_dim)
        linspaced_params.append(list(linspaced_param))

    if len(dims) != 1:
        # general case for n dimensions
        # compute the product of the linspace arrays indexes
        linspaced_params_idx = list(
            itertools.product(*[list(range(len(linspaced_param))) for linspaced_param in linspaced_params]))
        # compute the product of the linspace arrays
        linspaced_params_prod = list(itertools.product(*linspaced_params))
    else:
        # specific case for 1d dim
        # just take the the first dim linspace. No need for product
        linspaced_params_prod = linspaced_params[0]
        # jsut take the index of the first dim linspace
        linspaced_params_idx = [[elem] for elem in list(range(len(linspaced_params_prod)))]
    return linspaced_params_prod, linspaced_params_idx


def __d__(series, i, j):
    return abs(series[i] - series[j])


def __get_time_series__(pychaos, chaotic_system, param_combinations, dims,time_series_file, take_num, take_last, no_transform):
    param_list = []
    for param_combination in param_combinations:
        # get the parameters of the chaotic system from the api
        param = chaotic_system.get_params().copy()

        # change the ones that have been selected to be changed over time
        for i in range(len(dims)):
            param[dims[i]] = param_combination[i]

        if not no_transform:
            # transform parameter to include the influence of points of interest
            transformed_param = sensitivity_calibration.transform_custom_values(param, list(pychaos.synth.chaotic_sliders_dict.values()), pychaos.synth.get_pois())
            param_list.append(transformed_param)
        else:
            param_list.append(param)
    # api call to record the chaotic system with the parameter list
    if take_last:
        last = take_num
        first=0
        return sc_helper.record_chaotic_system(pychaos.sc, pychaos.sr, chaotic_system, param_list, time_series_last_file=time_series_file, take_last=last, take_first=first, enable_tqdm=True)[1]
    else:
        last = 0
        first = take_num
        return sc_helper.record_chaotic_system(pychaos.sc, pychaos.sr, chaotic_system, param_list, time_series_first_file=time_series_file, take_last=last, take_first=first, enable_tqdm=True)[0]


def __check_for_existing__data__(dims, points_per_dim, take_num, long_id):
     # create the full path and filename of the file where the dimension info is stored
    directory = os.path.join(config.DATA_DIR, "box_count_data")
    if not os.path.exists(directory):
        os.makedirs(directory)

    data_mapping_file = os.path.join(directory, 'mapping_file.json')
    if os.path.isfile(data_mapping_file):

        with open(data_mapping_file, "r") as json_file:
            # Load the JSON data into a dictionary
            id_dict = json.load(json_file)
        for key in id_dict.keys():
            if key == long_id:
                dimension_array_file = os.path.join(directory, id_dict[key] + '.npy')
                time_series_array =  os.path.join(directory, id_dict[key] + '.mymemmap')
                dimensions_array = np.load(dimension_array_file)
                time_series_array = np.memmap(time_series_array, dtype='float32', mode='r', shape=(points_per_dim * len(dims), take_num))
                return dimensions_array, time_series_array
    return None