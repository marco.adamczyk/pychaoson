import numpy as np
import itertools
import pychaoson.config as config

class SlidableParameter:
    """
    This class represents a variable that can be controled by a synth. It further creates customizable 
    linspaces that are used for the midi control data. This class particularly is used for the parameters of the chaotic system.
    """
    def __init__(self, start_value, min_value, max_value, linear_sensitivity, reverse, name, key_list):
        self.linspace = None
        self.start_value = start_value
        self.current_value_dict = dict()
        for key in key_list:
            self.current_value_dict[key] = start_value
        self.linear_sensitivity = linear_sensitivity
        self.max_delta = linear_sensitivity * (max_value - min_value)
        self.min_value = min_value
        self.max_value = max_value
        self.reverse = reverse
        self.name = name
        self.centered = True
        self.linspace_value_count = 50
        self.attached = False

    def set_attached(self, attached):
        self.attached = attached
    def get_attached(self):
        return self.attached
    
    def set_centered(self, centered):
        self.centered = centered
    
    def get_centered(self):
        return self.centered
        
    def set_linspace_value_count(self, linspace_value_count):
        self.linspace_value_count = linspace_value_count

    def set_linear_sensitivity(self, linear_sensitivity):
        self.linear_sensitivity = linear_sensitivity
        self.max_delta = linear_sensitivity * (self.max_value - self.min_value)

    def get_linear_sensitivity(self):
        return self.linear_sensitivity
        
    def get_max_delta(self):
        return self.max_delta

    def get_reverse(self):
        return self.reverse
    
    def set_reverse(self, reverse):
        self.reverse = reverse

    def get_min_value(self):
        return self.min_value

    def get_max_value(self):
        return self.max_value

    def get_current_value(self, key):
        return self.current_value_dict[key]

    def set_current_value(self, key, value):

        self.current_value_dict[key] = value

    def get_start_value(self):
        return self.start_value

    def set_start_value(self, start_value):
        self.start_value = start_value

    def set_current_value_to_start_value(self):
        for key in self.current_value_dict.keys():
            self.current_value_dict[key] = self.start_value

    def compute_linspace(self):
        if self.centered:
            self.compute_linspace_center(self.linspace_value_count)
        else:
            self.compute_linspace_edge(self.linspace_value_count)

    def compute_linspace_edge(self, value_count):
        if self.reverse:
            finish_value = self.start_value - self.max_delta
            if finish_value < self.min_value:
                finish_value = self.min_value
            self.linspace = np.linspace(self.start_value, finish_value, value_count).tolist()
        else:
            finish_value = self.start_value + self.max_delta
            if finish_value > self.max_value:
                finish_value = self.max_value
            self.linspace = np.linspace(self.start_value, finish_value, value_count).tolist()

    def compute_linspace_center(self, value_count):
        value_count = int(((value_count + 1) / 2))
        if self.reverse:
            lower_linspace = np.linspace(self.start_value + self.max_delta, self.start_value, value_count).tolist()[
                             :(-1)]
            lower_linspace = [self.max_value if value > self.max_value else value for value in lower_linspace]
            middle_linspace = [self.start_value]
            upper_linspace = np.linspace(self.start_value, self.start_value - self.max_delta).tolist()[1:]
            upper_linspace = [self.min_value if value < self.min_value else value for value in upper_linspace]
            self.linspace = lower_linspace + middle_linspace + upper_linspace
        else:
            lower_linspace = np.linspace(self.start_value - self.max_delta, self.start_value, value_count).tolist()[
                             :(-1)]
            lower_linspace = [self.min_value if value < self.min_value else value for value in lower_linspace]
            middle_linspace = [self.start_value]
            upper_linspace = np.linspace(self.start_value, self.start_value + self.max_delta, value_count).tolist()[1:]
            upper_linspace = [self.max_value if value > self.max_value else value for value in upper_linspace]
            self.linspace = lower_linspace + middle_linspace + upper_linspace

    def get_linspace(self):
        return self.linspace

    def get_name(self):
        return self.name


class CustomSlidableParameter(SlidableParameter):
    """
    Wraps the SlidableParameter class and adds a synth mapping variable. This makes it usable for controling custom synth variables.
    """
    def __init__(self, start_value, min_value, max_value, linear_sensitivity, reverse, name, synth_mapping_variable, key_list):
        super().__init__(start_value, min_value, max_value, linear_sensitivity, reverse, name, key_list)
        self.synth_mapping_variable = synth_mapping_variable

    def get_synth_mapping_variable(self):
        return self.synth_mapping_variable


def map_chaos_parameters_to_slides(chaotic_system):
    chaos_slides = []
    param_space_list = chaotic_system.get_param_space()
    for i in range(len(param_space_list)):
        param_space = param_space_list[i]
        chaos_slides.append(SlidableParameter(param_space[0],
                                              param_space[1],
                                              param_space[2],
                                              0.1,
                                              False,
                                              param_space[3],
                                              key_list = config.get_midi_note_list()))
    return chaos_slides


def get_custom_grain_len_slider():
    return (CustomSlidableParameter(0.05, 0.02, 0.2,
                                                linear_sensitivity=1,
                                                reverse=True,
                                                name="Grain Length",
                                                synth_mapping_variable='grainDur',
                                                key_list = config.get_midi_note_list()))
def get_custom_overlap_slider():
    return (CustomSlidableParameter(1, 0.5, 2,
                                                linear_sensitivity=1,
                                                reverse=True,
                                                name="Grain Overlap",
                                                synth_mapping_variable='overlap',
                                                key_list = config.get_midi_note_list()))
def get_custom_reverb_mix_slider():
    return (CustomSlidableParameter(0.3, 0.0, 1.0,
                                        linear_sensitivity=1,
                                        reverse=False,
                                        name="Reverb Mix",
                                        synth_mapping_variable="mix",
                                        key_list = config.get_midi_note_list()))
def get_amp_slider():
    return (CustomSlidableParameter(1.0, 0.0, 1.5,
                                        linear_sensitivity=1,
                                        reverse=False,
                                        name="Amp",
                                        synth_mapping_variable="amp", 
                                        key_list = config.get_midi_note_list()))

def get_all_accessible_params(sliders_dict):
    linspaced_params = []
    for key in sliders_dict.keys():
        slider = sliders_dict[key]
        if slider.get_attached() and type(slider) == SlidableParameter:
            linspaced_params.append(slider.get_linspace())
        else:
            linspaced_params.append([slider.get_start_value()])
    if len(linspaced_params) != 0:
        if len(linspaced_params) != 1:
            linspaced_params_prod = list(itertools.product(*linspaced_params))
        else: 
            linspaced_params_prod = linspaced_params[0]
        # apply the transform
    return linspaced_params_prod


def get_sliders_dict_list(sliders_dict):
    slides_dict_list = []
    for key in sliders_dict.keys():
        slider = sliders_dict[key]
        slide_dict = dict()
        slide_dict['value'] = slider.get_start_value()
        slide_dict['sensitivity'] = slider.get_linear_sensitivity()
        slide_dict['reverse'] = slider.get_reverse()
        slide_dict['name'] = slider.get_name()
        slides_dict_list.append(slide_dict)
    return slides_dict_list


def get_custom_slide_control_dict(custom_slides):
    control_dict = {}
    for custom_slide in custom_slides:
        control_dict[custom_slide.get_synth_mapping_variable()] = custom_slide.get_start_value()
    return control_dict
