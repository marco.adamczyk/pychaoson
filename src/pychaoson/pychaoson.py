import pychaoson.utilities as utilities
import pychaoson.sample_edit as sample_edit
import pychaoson.config as config
import pychaoson.chaotic_systems as chaotic_systems
import pychaoson.roli_control as roli_control
import pychaoson.synth as synth
import pychaoson.sensitivity_calibration as sensitivity_calibration
import pychaoson.sliders as sliders
import pychaoson.exceptions as exceptions
import json
import os
import sc3nb as scn


def startup(input_device=None, output_device=None, data_path=None, samples_path=None):
    """
    Init the main api interface that manages the execution of synths. An available sc3 installation is required

    Parameters
    ----------
    input_device : str, optional
        The device to get the audio from. Can be used for routing sound on mac and windows. 
        Devices can be found in sc3 with ServerOptions.inDevices. If not selected the default is chosen.
    output_device : str, optional
        The device to get the audio from. Can be used for routing sound on mac and windows. 
        Devices can be found in sc3 with ServerOptions.outDevices. If not selected the default is chosen.
    data_path: str, optional
        The path to the directory where the computed data is going to be saved.
    samples_path: str, optional
        The path to the directory where the sample are stored
    
    Returns
    -------
    pychaoson: Pychaoson
        SuperCollider Interface class.
    """
    if input_device is not None or output_device is not None:
        scn.ServerOptions(hardware_input_device=input_device, hardware_output_device=output_device)
    sc = scn.startup(console_logging=False)
    # set directories
    if data_path is not None:
        config.DATA_DIR = data_path
    if samples_path is not None:
        config.SAMPLES_DIR = samples_path
    # return instance
    return Pychaoson(sc)



class Pychaoson:
    """
    The object for managing the synths, configurations, 
    connection to inputs and providing data for visualization purposes
    """

    def __init__(self, sc):
        """
        Init the main api interface that manages the execution of synths. An available sc3 installation is required. Loads
        the chaotic sytstems, loads the available systems from resources and pitches them into the preset midi value space.

        Parameters
        ----------
        sc : SC
            default SC instance
        """
        self.__init_data_folders__()
        # set the sc object and get the samplerate
        self.sc = sc
        self.sr = int(sc.server.nominal_sr)
        self.roli_control = None # connected data input
        self.synth = None # set synth
        # available samples
        self.sample_files = utilities.load_files_from_directory(config.SAMPLES_DIR)
        self.samples_dict = sample_edit.get_samples_dict(self.sample_files, self.sr, get_files=True)  
        self.buffer_dict = dict()

        # available sythnsesis methods
        self.playtype_names = ['Granular', 'Chaotic']
        # available chaotic systems
        self.chaotic_systems_dict = chaotic_systems.load_kr_systems()
        # update synth flag
        self.update_synth = False

    def start_synth(self, chaotic_system_name, playtype, sample_name=None):
        """
        Initialzes sc3nb synthdef for audio synthesis with the selected chaotic system and synthesis type. If the synthesis type is set
        to Granular a sample name is required. This method works if a synth is already initialized. It is going to be overwritten.

        Parameters
        ----------
        chaotic_system_name : str
            The name of the chosen chaotic system
        playtype : str
            The name of the synthesis type
        sample_name : str, optional
            The name of the sample

        Returns
        -------
        Synth
            The synth interface class
        """
        if chaotic_system_name not in self.chaotic_systems_dict.keys():
            raise exceptions.NoSuchChaoticSystemException('The chaotic sytem "{}" is not available')
        if playtype == 'Granular' and sample_name == None:
            raise ValueError('The sample_name cannot be None, if playtype is the to Granular.')
        self.update_synth = True
        if self.synth is not None:
            if self.roli_control is not None: 
                # stop midi processing
                self.roli_control.stop_midi_processing()
            if self.synth.chaotic_system.get_name() == chaotic_system_name and self.synth.playtype == playtype and playtype == 'Granular':
                # only update the sample
                self.synth.set_sample(sample_name)
            else:
                if self.roli_control is not None: 
                    self.roli_control.unset_all_roli_control()
                for key in self.synth.synth_def_dict.keys():
                    self.synth.synth_def_dict[key].free()
                # do a full synth update
                self.synth = synth.Synth(self.sc, 
                                        self.chaotic_systems_dict[chaotic_system_name],
                                        playtype, 
                                        self.samples_dict,
                                        sample_name,
                                        self.buffer_dict)
            if self.roli_control is not None: 
                self.roli_control.set_synth(self.synth)
                self.roli_control.run_midi_processing()
        else:
            self.synth = synth.Synth(self.sc, 
                                     self.chaotic_systems_dict[chaotic_system_name],
                                     playtype, 
                                     self.samples_dict,
                                     sample_name,
                                     self.buffer_dict)
        self.update_synth = False
        return self.synth

    def load_patch(self, patch_name):
        """
        Loads a patch to the current synth. A patch consists of chaotic system, synthesis method, param start values,
        optional sample name and roli mappings

        Parameters
        ----------
        patch_name : str
            The patch name. The name of the save file in the patch data folder.
        """
        # read the json file from the patch data directory
        json_patch_path = os.path.join(config.PATCH_DATA_DIR, patch_name + '.json')
        with open(json_patch_path, 'r') as openfile:        
            # Reading from json file
            json_patch = json.load(openfile)
            if json_patch['sample'] == 'None':
                json_patch['sample'] = None
            self.start_synth(json_patch['chaotic_map'], json_patch['playtype'], json_patch['sample'])
            sliders_dict = self.synth.all_sliders_dict
            json_sliders = json_patch['slides']
            for json_slider in json_sliders:
                name = json_slider['name']
                sliders_dict[name].set_start_value(json_slider['value'])
                sliders_dict[name].set_reverse(json_slider['reverse'])
                sliders_dict[name].set_linear_sensitivity(json_slider['sensitivity'])
                sliders_dict[name].set_current_value_to_start_value()
                sliders_dict[name].compute_linspace()
            

            roli_dict = json_patch['roli']
            self.roli_control.unset_all_roli_control()
            if roli_dict['x'] is not None:
                self.roli_control.set_roli_control('x', roli_dict['x'])
            if roli_dict['y'] is not None:
                self.roli_control.set_roli_control('y', roli_dict['y'])
            if roli_dict['z'] is not None:
                self.roli_control.set_roli_control('z', roli_dict['z'])
            
            self.synth.__precompute_chaos_output__()

            pois = json_patch['poi']

            for poi in pois:
                self.synth.add_poi(poi['poi'], poi['radius'], poi['exp'], False)


    def save_patch(self, patch_name):
        """
        Loads settings like chaotic system, synthesis method, start param values and optional 
        sample name and roli mappings from a json file in the patch data folder.

        Parameters
        ----------
        patch_name : str
            The patch name. The name of the file that should be saved as json
        """
        # save all the neccessary information as a dict
        if self.synth is not None:
            patch_dict = dict()
            patch_dict['chaotic_map'] = self.synth.chaotic_system.get_name()
            if self.synth.current_sample_name is None:
                patch_dict['sample'] = 'None'
            else:
                patch_dict['sample'] = self.synth.current_sample_name
            if self.roli_control is None:
                patch_dict['roli'] = 'None'
            else:
                patch_dict['roli'] = self.roli_control.get_mapping_dict()
            patch_dict['slides'] = sliders.get_sliders_dict_list(self.synth.all_sliders_dict)
            patch_dict['poi'] = sensitivity_calibration.get_points_of_interest_dict(self.synth.get_pois())
            patch_dict['playtype'] = self.synth.playtype
            # convert this dictionary to a json file
            json_patch = json.dumps(patch_dict, indent=4)
            json_patch_path = os.path.join(config.PATCH_DATA_DIR, patch_name + '.json')
            # save it in the patch data folder
            with open(json_patch_path, 'w') as outfile:
                outfile.write(json_patch)
        else:
            raise Exception('Start a synth first')
        
    def delete_patch(self, patch_name):
        """
        Deletes the save patch file from patch directory.

        Parameters
        ----------
        patch_name : str
            The patch name. The name of the file that should be removed
        """
        json_patch_path = os.path.join(config.PATCH_DATA_DIR, patch_name + '.json')
        os.remove(json_patch_path)

    def get_available_patches(self):
        """
        Gets all the available patches in the patches folder

        Return
        ----------
        patch_list : [str]
            A list of the patch names
        """
        # get all file names in the patch data folder
        return utilities.get_file_names_from_dir(config.PATCH_DATA_DIR)

    def get_chaotic_system_names(self):
        """
        Gets all the available chaotic system names.

        Return
        ----------
        patch_list : [str]
            A list of the chatic system names.
        """
        return list(self.chaotic_systems_dict.keys())
    
    def get_play_types(self):
        """
        Gets all the available synthesis methods.

        Return
        ----------
        patch_list : [str]
            A list of the available synthesis mehtods.
        """
        return self.playtype_names

    def get_sample_names(self):
        """
        Gets all the available sample names.

        Return
        ----------
        patch_list : [str]
            A list of the available sample names.
        """
        return list(self.samples_dict.keys())

    def init_roli_control(self, port_name):
        """
        Initialzes the connection to a roli seaboard and starts the midi processing loop for input.

        Parameters
        ----------
        port_name : str
            The name of the midi port of the device

        Returns
        -------
        RoliControl
            The roli control interface class
        """
        if self.synth != None:
            if self.roli_control is None:
                self.roli_control = roli_control.RoliMidiProcessing(self.synth, port_name)
                self.roli_control.run_midi_processing()
        else:
            raise Exception('Start a synth first')
        return self.roli_control
    
    def delete_roli_control(self):
        """
        Deletes the current roli conection and stops the synth control 
        """
        if self.roli_control is not None:
            self.roli_control.stop()
            self.roli_control = None

    def stop(self):
        """
        Delete roli conection and stop sc.
        """
        self.delete_roli_control()
        self.sc.exit()


    def __init_data_folders__(self):
        os.makedirs(config.PATCH_DATA_DIR, exist_ok=True)
        os.makedirs(config.RECORD_DATA_DIR, exist_ok=True)
        os.makedirs(config.SAMPLE_DATA_DIR, exist_ok=True)