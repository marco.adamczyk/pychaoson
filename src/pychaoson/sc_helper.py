import sc3nb as scn
from sc3nb import Buffer, SynthDef, Score
import pychaoson.chaotic_systems as chaotic_systems
import numpy as np
from tqdm import tqdm
import pychaoson.utilities as util
import pychaoson.config as config
import os
import soundfile as sf
import sc3nb.sc_objects.server


def record_chaotic_system(sc, sr, chaotic_system, chaos_param_list, time_series_last_file=None, time_series_first_file=None, timing_delta=0.05, take_last=300, take_first=100, enable_tqdm=True, prefix_file_name='score_',sample_format='int32' ):
    """
    Records chaotic system by sc3 non realtime synthesis. Values are stored in wav file and the read back.

    Parameters
    ----------
    sc : SC
        An SC instance
    sr : int
        The sample rate to record the data.
    chaotic_system : ChaoticSystem
        A chaotic system instance
    chaos_param_list : [[floats]]
        A list of value combinations of the chaotic system that should be recorded
    time_series_last_file : str, optional
        The np memory map file to which the last record results should be written. If none no memory map is created
    time_series_first_file : str, optional
        The np memory map file to which the first record results should be written. If none no memory map is created
    timing_delta : float, optional
        The time for every parameter combination to be played by the synth. The length determines how many values are recorded and therefore how much can be taken
        The length of recorded values per combination is timing_delta * sr
    take_last : int, optional
        The number of the value that should be take at the end of the recorded value array
    take_first : int, optional
            The number of the value that should be take at the beginning of the recorded value array. 
    enable_tqdm : bool, optional
        Determines if the progress of the recording should be printed to the console
    prefix_file_name : str, optional
        The names of the wav files the chaotic system output is saved t
    sample_format : str, optional
        The precision the recorded float values
    
    Returns
    -------
    time_series_array: (np.array, np.array)
        Two 2D numpy arrays that store the recorded values
    """
    chaos_param_list = chaos_param_list.copy()
    selected_chaotic_system = chaotic_system
    # define sclang code for the synthesiyer
    synthdef_context = SynthDef(
        "recorder",
        r"""{ | {{INIT_VALUES}} out, freq, chaosParams=#{{CHAOS_PARAMS}}|
                Out.ar(out,
                    {{GRANULAR}} * Line.ar(1, 1, {{TIMING_DELTA}}, doneAction: Done.freeSelf)
                )
            }""",
    )
    # set the context of the synth definition
    synthdef_context.set_context("CHAOS_PARAMS", selected_chaotic_system.get_params())
    synthdef_context.set_context("GRANULAR", selected_chaotic_system.get_sc_generator_for_grains())
    synthdef_context.set_context("TIMING_DELTA", str(timing_delta))
    synthdef_context.set_context("INIT_VALUES", chaotic_systems.get_init_values_as_args(selected_chaotic_system))
    take_last_dimension = (len(chaos_param_list), take_last)
    take_first_dimension = (len(chaos_param_list), take_first)
    # create a file backed numpy array if a file is specified
    if time_series_last_file != None:
        time_series_last_array = np.memmap(time_series_last_file, dtype='float32', mode='w+', shape=take_last_dimension)
    else:
        time_series_last_array = np.zeros(dtype='float32', shape=take_last_dimension)
    if time_series_first_file != None:
        time_series_first_array = np.memmap(time_series_first_file, dtype='float32', mode='w+', shape=take_first_dimension)
    else:
        time_series_first_array = np.zeros(dtype='float32', shape=take_first_dimension)


    chunk_size_per_file = 1000
    index = 0
    # split the chaos_param list into chunks to save the results in seperate files
    chaos_param_list_chunks = util.divide_chunks(chaos_param_list, chunk_size_per_file)
    # iterate over chaos parameter chunks 
    if enable_tqdm:
        chaos_param_list_chunks = tqdm(chaos_param_list_chunks, desc="Compute time series:")
    for chaos_param_list_chunk in chaos_param_list_chunks:
        take_first_recorded, take_last_recorded = __record_wav_file__(sc, sr, timing_delta,prefix_file_name + str(index) + ".wav", prefix_file_name + str(index) + ".osc",
                                    synthdef_context,
                                    chaos_param_list_chunk, take_first, take_last, sample_format)
        time_series_first_array[index * chunk_size_per_file:index * chunk_size_per_file + len(chaos_param_list_chunk),
        :] = take_first_recorded
        time_series_last_array[index * chunk_size_per_file:index * chunk_size_per_file + len(chaos_param_list_chunk),
        :] = take_last_recorded
        index += 1
    # if the result array is a memmap then write the results to the file
    if time_series_first_file != None:
        time_series_first_array.flush()
    if time_series_last_file != None:
        time_series_last_array.flush()
    return time_series_first_array, time_series_last_array


def play_buffer(sc, bufnum):
    """
    Plays a sc3 buffer.

    Parameters
    ----------
    sc : SC
        An SC instance
    bufnum : int
        The buffer number of the buffer
    """
    # execute a language command in supercollider as string
    sc.lang.cmd('Buffer(s, bufnum: ' +
                        str(bufnum) +
                        ', numChannels: 1).play()')


def get_chaotic_synth_def():
    """
    Initializes a sc3nb synthdef for the chaotic synthesis method

    Return
    ----------
    synthdef : SynthDef
        An sc3nb SynthDef instance
    """
    return scn.SynthDef(name="chaotic", definition=r"""
            {   | {{INIT_VALUES}} density=10, chaosParams=#{{CHAOS_PARAMS}}, freq={{FREQENCY}} ,gate=0, rate=1, amp=0.5, mix = 0.5, room = 0.4, damp = 0.5, out=0 |
                var sig, env;
                env = EnvGen.kr(Env.adsr(attackTime: 0.01, decayTime: 0.3, sustainLevel: 0.5, releaseTime: 0.3, peakLevel: 1.0, curve: -4.0, bias: 0.0), gate);
                sig = {{VALUEGENERATOR}};
                sig = sig * env * amp;
                sig = FreeVerb.ar(
                    sig,
                    mix,
                    room,
                    damp
                );
                sig = [sig, sig];
                sig = Pan2.ar(sig, 0.5);
                FreeSelf.kr(TDelay.kr(Done.kr(env),5));
                Out.ar(out, sig);
            }""")


def get_granular_synth_def():
    """
    Initializes a sc3nb synthdef for the granular synthesis method

    Return
    ----------
    synthdef : SynthDef
        An sc3nb SynthDef instance
    """
    return scn.SynthDef(name="granular", definition=r"""
            {   | {{INIT_VALUES}} density=10, chaosParams=#{{CHAOS_PARAMS}}, overlap=1, bufnum=0, grainDur=0.01, useSeq=0, cycleBufnum=0, startBufFrame=0, endBufFrame=0, gate=0, rate=1, amp=0.5, int=2, pan=0, envBuf=(-1), maxGrains=512, mix = 0.5, room = 0.4, damp = 0.5, out=0 |
                var sig, env, buf, freq, grainSelect;
                freq = (1 / grainDur) * (round(overlap *10)/10);
                buf = Buffer(s, bufnum: bufnum, numChannels: 1);
                env = EnvGen.kr(Env.adsr(attackTime: 0.01, decayTime: 0.3, sustainLevel: 0.5, releaseTime: 0.3, peakLevel: 1.0, curve: -4.0, bias: 0.0), gate);
                grainSelect = Select.kr(useSeq, [{{GRAINSELECTOR}} , BufRd.ar(1, cycleBufnum, Phasor.ar(0, (freq / SampleRate.ir), startBufFrame, endBufFrame + 1), 1, 1)]);
                sig = GrainBuf.ar(2, Impulse.ar(freq, phase: 0), grainDur, buf, rate, grainSelect * 0.90, int, pan, envBuf, maxGrains, amp);
                sig = sig * env * amp;
                sig = FreeVerb.ar(
                    sig,
                    mix,
                    room,
                    damp
                );
                FreeSelf.kr(TDelay.kr(Done.kr(env),5));
                Out.ar(out, sig);
            }""")


def __record_wav_file__(sc, sr, timing_delta, file_name_wav, file_name_osc, synthdef, chaos_param_list, take_first, take_last, sample_format):
    timing = 0.0
    data_len_per_configuration = int(timing_delta * sr)
    with sc.server.bundler(send_on_exit=False, ) as bundler:
        synthdef.add()
        nodeID = 100
        for chaos_param in chaos_param_list:
            # Send the test SynthDef
            with util.suppress_stdout():
                bundler.add(timing, "/s_new",
                            ["recorder", nodeID, 0, 0, "freq", int(sr), "chaosParams", list(chaos_param)])
            timing += timing_delta
            nodeID += 1
        # The /c_set [0, 0] will close the audio file
        with util.suppress_stdout():
            bundler.add(timing, "/c_set", [0, 0])
    #with util.suppress_stdout():
    with util.suppress_stdout():
        Score.record_nrt(bundler.messages(), os.path.join(config.RECORD_DATA_DIR, file_name_osc),
                            os.path.join(config.RECORD_DATA_DIR, file_name_wav), sample_rate=sr,
                            header_format="WAV", sample_format=sample_format, options=sc3nb.sc_objects.server.ServerOptions(num_output_buses=1, block_size=0 ))
    #self.sc_server.server.free_all()
    data, _ = sf.read(os.path.join(config.RECORD_DATA_DIR, file_name_wav))

    time_series_list = list(util.divide_chunks(data, data_len_per_configuration))
    if len(time_series_list[-1]) < data_len_per_configuration:
        del time_series_list[-1]
    time_series_array_last = np.array([time_series[data_len_per_configuration - take_last:]
                                    for time_series in time_series_list])
    time_series_array_first = np.array([time_series[:take_first]
                            for time_series in time_series_list])
    return time_series_array_first, time_series_array_last
