import librosa
import numpy as np
import os
import soundfile
from tqdm import tqdm

import pychaoson.utilities as utilities
import pychaoson.config as config


def estimate_fundamental_freq(y, sample_rate):    
    """
    Estimates the average fundamental freq of a sound with the pyin algorithm

    Parameters
    ----------
    y : [float]
        The input audio data
    sample_rate : int
        The sample rate of the sound data
    
    Returns
    -------
    avg_fundamental: int
        The fundamental freq of the input sound data
    """
    f0, _, _ = librosa.pyin(y, fmin=librosa.note_to_hz('C2'),
                                                 fmax=librosa.note_to_hz('C7'),
                                                 sr=sample_rate)
    masked_f0 = np.ma.masked_array(f0, np.isnan(f0))

    return np.average(masked_f0)


def get_samples_dict(sample_files, sr, reload=False, get_files=False):
    """
    Computes or loads the pitched samples for every provided sample. 

    Parameters
    ----------
    samples_file : str
        The input audio files
    sr : int
        The sample rate of the sound data
    reload : str
        If set to False, it loads the data from the files if already computed in the past. If set to true, it computes the data regardless if its already saved
    get_files : int
        If set to False the data of the pitched sound files is returned, if true only the paths to the files
    
    Returns
    -------
    sample_dict: dict
        A dictionary that has the sample names as keys and the dict of the pitched sample as value
    """
    samples_dict = dict()
    for sample_file in sample_files:
        samples_dict[utilities.get_filename_of_path(sample_file)] = __pitch_sample__(sample_file,
                                                                                      sr,
                                                                                      reload,
                                                                                      get_files)
    return samples_dict


def __pitch_sample__(sample_file, sr, reload=False, get_files=False):
    """
    Pitch a sample file to all the different midi note pitches that are determined in the config file. Saves the pitch files as wav files in the data folder.
    If the method is called again, it loads the saved files.

    Parameters
    ----------
    sample_file : str
        The input audio data
    sr : int
        The sample rate of the sound data
    reload : str
        If set to False, it loads the data from the files if already computed in the past. If set to true, it computes the data regardless if its already saved
    get_files : int
        If set to False the data of the pitched sound files is returned, if true only the paths to the files
    
    Returns
    -------
    sample_pitch_dict: dict
        Returns the sound data of every pitch. If get_files param is set to true, then returns the path to the soundfiles for every pitch
    """
    sample_filename = utilities.get_filename_of_path(sample_file)
    pitched_samples_dir = os.path.join(config.SAMPLE_DATA_DIR, sample_filename
                                       + "_pitches_" + str(sr))
    # create a directory that contains all the pitches version of the original. The keys are the midi note
    sample_pitch_dict = {}
    if not os.path.isdir(pitched_samples_dir) or reload:

        os.makedirs(pitched_samples_dir, exist_ok=True)

        # load the existing file that has a
        y, _ = librosa.load(sample_file, sr=sr)

        # get fundamental frequency
        avg_f0 = estimate_fundamental_freq(y, sr)
        print("Found average f0: " + str(avg_f0))

        # pitch to closest midi note in defined range
        actual_midi = librosa.hz_to_midi(avg_f0)
        closest_midi_note = np.round(actual_midi)

        if closest_midi_note < config.LOWEST_MIDI_NOTE:
            closest_midi_note = config.LOWEST_MIDI_NOTE
        elif closest_midi_note > config.HIGHEST_MIDI_NOTE:
            closest_midi_note = config.HIGHEST_MIDI_NOTE

        librosa.effects.pitch_shift(y=y, sr=sr, n_steps=abs(actual_midi - closest_midi_note))

        # create pitched versions
        for current_midi_note in tqdm(range(config.LOWEST_MIDI_NOTE, config.HIGHEST_MIDI_NOTE + 1), desc="Pitch Sample: " +
                                                                                           sample_filename):
            # pitch effect
            sample_pitch_dict[current_midi_note] = \
                librosa.effects.pitch_shift(y=y, sr=sr,
                                            n_steps=current_midi_note - closest_midi_note)

            # write to sound file in order to not compute again
            soundfile.write(os.path.join(pitched_samples_dir, str(current_midi_note)) + '.wav',
                            sample_pitch_dict[current_midi_note], sr)

    # load sound files
    pitched_sounds = utilities.load_files_from_directory(pitched_samples_dir)
    if not len(pitched_sounds) == config.HIGHEST_MIDI_NOTE - (config.LOWEST_MIDI_NOTE - 1):
        raise Exception("Not the right notes. Clean directory")

    for pitched_sound in pitched_sounds:
        y, _ = librosa.load(os.path.join(pitched_samples_dir, pitched_sound), sr=sr)
        pitched_sound_name = utilities.get_filename_of_path(pitched_sound)
        # name of the file should correspond to the midi_note
        midi_note = int(pitched_sound_name)

        # either get the values directly or get the file path where they were written to
        if get_files:
            sample_pitch_dict[midi_note] = os.path.join(pitched_samples_dir, pitched_sound)
        else:
            sample_pitch_dict[midi_note] = y

    return sample_pitch_dict
